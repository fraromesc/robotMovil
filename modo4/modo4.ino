//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDel 52
#define echoDel 50

#define trigTras 25
#define echoTras 22

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que  avanzando hacia izda o dcha a cierta velocidad

//PARÁMETROS DEL CONTROL 
float Ureal=0;                  //Señal de control que se aplica
float Umin=90.0/110.0;          //Señal de control mínima
float Umax=130.0/110.0;         //Señal de control máxima
float Uk_ss=0;                  //Señal de control intermedia (antes del anti-windup)
float Ueq=1;                    //Valor de equilibrio de la señal de control
float ek=0;                     //Error en k
float ek1=0;                    //Error en k-1
float distTras=0;               //Distancia medida por el ultrasonidos izquierdo
float distDel=0;                //Distancia medida por el ultrasonidos derecho
float ekDel=12;                 //Error en k del motor derecho 
float ekTras=12;                //Error en k del motor izquierdo
float Kp=0.07;                  //Cte proporcional para el control 
float Ti=200;                   //Tiempo integral para el control
float Td=0.02;                  //Tiempo derivativo para el control  
float uk=0;                     //Señal de control instante k 
float Tm=0.01;                  //Periodo de muestreo EN SEGUNDOS
float Int_err=0;                //Integral del error
float ref=0;                    //Referencia para el control
unsigned long t=0;              //Variable de tiempo para esperar Tm
unsigned long t2=0;             //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;             //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;             //Variable para calcular el tiempo entre lecturas del bluetooth para enviar por telemetría como primer dato
float ajuste = 1;               //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;              //Estado para una pseudomáquina de estados que decide entre aplicar y no aplicar la acción de control a los motores   
bool telemetria =false; 
unsigned char recepcion = 185;
float distUS=11.5;              //Distancia física entre los dos US 
int modoTest=0;                 //Variable para la máquina de estados de la referencia del control de distancia
float theta;

//PARÁMETROS DEL CONTROL DE DISTANCIA
float UrealDist=0;              //Señal de control que se aplica
float UminDist=-10;             //Señal de control mínima
float UmaxDist=10;              //Señal de control máxima
float Uk_ssDist=0;              //Señal de control intermedia (antes del anti-windup)
float UeqDist=0;                //Valor de equilibrio de la señal de control
float ekDist=0;                 //Error en k
float ek1Dist=0;                //Error en k-1
float KpDist=2;                 //Cte proporcional para el control 
float TiDist=200;               //Tiempo integral para el control
float TdDist=0;                 //Tiempo derivativo para el control  
float ukDist=0;                 //Señal de control instante k 
float Int_errDist=0;            //Integral del error
float refDist=50;               //Referencia para el control

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigTras,OUTPUT);
  pinMode(echoTras,INPUT);
  pinMode(trigDel,OUTPUT);
  pinMode(echoDel,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);
}

 
void loop() {

  //CONTROLADOR 
  if (millis()- t >= Tm*1000)                         //Esperar Tm (millis-t == tiempo desde que se ejecutó el control la última vez EN MILISEGUNDOS)
  {                                                   //Tm * 1000 = pasar Tm A MILISEGUNDOS

    //Medida de los ultrasonidos
    distTras=distanciaUS(trigTras, echoTras);
    distDel=distanciaUS(trigDel, echoDel); 
    theta = acos(distUS/sqrt((pow(distUS,2))+pow((-distDel+distTras),2))*1.0)*180/3.1415;  //Por definición de todos los elementos de la fórmula
                                                                                           //siempre da positivo
    if(distDel < distTras) theta = -theta;                                                 //Definiremos theta > 0 si distDel > distTras, y <0 en 
                                                                                           //caso contrario   


    //BUCLE EXTERNO
    //Cálculo del error para el controlador del bucle externo
    ekDist = refDist - (distTras+distDel)/2;
    //Implementación de controlador PID de Distancia
    ukDist = KpDist * ekDist + (Int_errDist / TiDist) + TdDist * ((ekDist - ek1Dist) / Tm);  //PID Completo
    Uk_ssDist = UeqDist+ukDist;
    //Saturación del controlador [UminDist = -10; UmaxDist = 10]
    if (Uk_ssDist < UminDist)UrealDist = UminDist;
    else if (Uk_ssDist > UmaxDist)UrealDist = UmaxDist;
    else UrealDist = Uk_ssDist; 
    if(Uk_ssDist<UmaxDist && Uk_ssDist>UminDist)Int_errDist += ekDist * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Paso de parámetro para bucle interno
    ref = UrealDist;
  
    //BUCLE INTERNO                                                                                        
    //Cálculo del error para el controlador interno
    ek=ref-theta;                                          //Cálculo del error para el controlador como diferencia entre el ángulo deseado y el medido
                               
    //Implementación de controlador PID
    uk = Kp * ek + (Int_err / Ti) + Td * ((ek - ek1) / Tm);  //PID Completo
    //if(uk>0 && uk<80/90)uk = 80/90;    //Salvar zona muerta por la zona positiva
    //if(uk<0 && uk>-80/(90*0.902))uk = -80/(0.902*90);  //Salvar zona muerta por la zona negativa
    Uk_ss = Ueq+uk;
    //Saturación del controlador [Umin = -1; Umax = 1]
    if (Uk_ss < Umin)Ureal = Umin;
    else if (Uk_ss > Umax)Ureal = Umax;
    else Ureal = Uk_ss; 
    if(Uk_ss<Umax && Uk_ss>Umin)Int_err += ek * Tm; //Anti-windup: sólo para cuando tenemos efecto integral
    
    //Actualizar variables
    ek1 = ek;
    ek1Dist = ekDist;
    t = millis();

    //Esperar en régimen permanente
    if(!((ekDist <= 2 && ekDist >= -2))) t3 = millis();         //Si el error NO es cero (error permitido 1 cm), 
                                                        //actualizar t3 a millis, si sí lo es, comenzar la espera de régimen permanente
  }


  //MAQUINA DE ESTADO PARA LOS CAMBIOS DE REFERENCIA
  switch (modoTest){
    case 0:
      refDist=30; 
      if (millis()-t3>2000) {
        modoTest++;
        t3=millis(); 
      }
    break; 
    case 1: 
      refDist=40; 
      if (millis()-t3>2000) modoTest++; 
    break;
    case 2: 
      modo = false; 
      break; 
  } 
      
    
  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 'm' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo){                              //Estado == 2 => Detener el coche pues se ha alcanzado la referencia final (50 cm)
 
      MotorIzquierdo(avanza, 110*Ureal); 
      MotorDerecho(avanza, 110*0.960);
         

  }
  else recto(avanza,detener); 


  //AJUSTE EN LÍNEA DE LAS CONSTANTES DEL CONTROL
  if (millis()- t2 >= 100){                             //Cada 100 ms, leer el teclado
   if (recepcion== 'q') KpDist+=ajuste;
   else if (recepcion=='a') KpDist-=ajuste; 
   else if (recepcion =='w') TiDist+=ajuste;
   else if (recepcion =='s') TiDist-=ajuste; 
   else if (recepcion =='e') TdDist+=ajuste;
   else if (recepcion =='d') TdDist-=ajuste;
   else if (recepcion =='r') refDist++;
   else if (recepcion =='f') refDist--;
   else if (recepcion =='1') ajuste*=10;
   else if (recepcion =='2') ajuste*=0.1;
   else if (recepcion == 'z') modoTest = 0;             //Esta tecla resetea el estado sin usar el botón de reset del arduino
   else if (recepcion == 'm') modo = !modo;             //Con cada pulsación de m, se alterna el modo
   else if (recepcion == 't') telemetria = !telemetria; 
   recepcion = 185;                                     //Resetear la tecla en memoria cuando no se esté pulsando
   t2 = millis();
  }

 
  //"TELEMETRÍA"
  /*
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  Serial1.print("USTras = ");
  Serial1.print(distTras);
  Serial1.print("   USDel = ");
  Serial1.print(distDel);
//  Serial1.print("   Ureal = ");
//  Serial1.print(Ureal);
  Serial1.print("   ref  =  "); 
  Serial1.print(refDist);
  Serial1.print("   thetaDes = ");
  Serial1.print(ref);
  Serial1.print("   theta = ");
  Serial1.print(theta);  
  Serial1.print("   Kp = ");
  Serial1.print(KpDist);
  Serial1.print("   Ti = ");
  Serial1.print(TiDist);
  Serial1.print("   Td = ");
  Serial1.print(TdDist);
  Serial1.print("   ajuste = ");
  Serial1.print(ajuste);
  Serial1.print("   estado = ");
  Serial1.print(recepcion);
  //Serial1.print("   t2=   "); 
  //Serial1.print(t2);
  Serial1.print("   modo=   "); 
  Serial1.print(modoTest);
  Serial1.println();*/
  // 

  //TELEMETRÍA
   
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  //Escribir en Putty
  //Tiempo
  if (telemetria){
    Serial1.print(millis()-t4);                  //dat1: tiempo entre lecturas de ultrasonidos
   Serial1.print(" ");
   //Sensores
   Serial1.print(distTras);                      //dat2: lectura del sensor izquierdo
   Serial1.print(" ");
   Serial1.print(distDel);                       //dat3: lectura del sensor derecho
   Serial1.print(" ");
   Serial1.print(refDist);                       //dat4: referencia para el control de distancia
   Serial1.print(" ");
   Serial1.print((distTras+distDel)/2);          //dat5: media de las dos lecturas de los ultrasonidos
   Serial1.print(" ");
   Serial1.print(theta);                         //dat6: ángulo que forma el coche con la pared
   Serial1.print(" "); 
   Serial1.print(ref);                           //dat7: referencia para el control de ángulo
   Serial1.print(" ");
   //Actuadores
   Serial1.print(Ureal);                         //dat8: escritura de la actuación (factor de corrección para el giro)
   Serial1.print(" "); 
   Serial1.print(modoTest);                      //dat9: variable para la máquina de estados de la referencia del control de distancia
   Serial1.println();
   }

   t4 = millis();                                //Actualizar t4 para el cálculo del tiempo entre lecturas del bluetooth
    
}

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.902*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}
