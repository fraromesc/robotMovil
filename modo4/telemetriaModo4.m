% Funci�n para mostrar telemetr�a de robot m�vil
% Laboratorio de Rob�tica 4� GIERM 18-19
% Federico Cuesta
% USO: Pasar como par�metro el nombre del archivo de telemetr�a con el
% sigiente contenido por fila:
% -Incremento de tiempo transcurrido desde la lectura anterior (milisegundos),  
% -Distancia medida por sensor Izq/Frontal (cm), 
% -Distancia medida por sensor Dch/Trasero(cm), 
% -Referencia de control (cm), 
% -Modo activo (0: Parado, 1: Control frontal, � 4),
% -Velocidad PWM motor Izq (+/-255, negativo indica marcha atr�s), 
% -Velocidad PWM motor Dch (+/-255, negativo indica marcha atr�s).

function telemetria(archivo)

tel=load('P4G08MODO4.txt');
muestras=length(tel);
disp('Incremento de tiempo m�nimo:'); disp(min(tel(:,1)));
disp('Incremento de tiempo m�ximo:');disp(max(tel(:,1)));
disp('Incremento de tiempo promedio:'); disp(mean(tel(:,1)));
tiempo=zeros(1,muestras);
tiempo(1)=tel(1,1); %Vector de tiempo absoluto
for i=2:muestras
    tiempo(i)=tiempo(i-1)+tel(i,1);
end    

% %MODO 3 
% subplot(3,1,1);
% plot( tiempo,tel(:,4),tiempo,tel(:,5));legend( "Theta", "ref");grid; 
% xlabel('Tiempo (ms)');
% title('Referencia (�ngulo) vs �ngulo del robot (�)');
% 
% subplot(3,1,2);
% plot(tiempo,tel(:,2), tiempo,tel(:,3));legend('SensorDel',"SensorTras");grid;
% xlabel('Tiempo (ms)');
% title('Sensores ultrasonidos (cm)');
% 
% subplot(3,1,3);
% plot( tiempo,tel(:,6));grid;%, tiempo,tel(:,7));legend('Motor Der',"Motor Izq");
% title('UReal = fCorr (tanto por uno)');
% xlabel('Tiempo (ms)');


%MODO 4 
subplot(3,1,1);
plot( tiempo,tel(:,2),tiempo,tel(:,3),tiempo,tel(:,4),tiempo,tel(:,5));legend( "DistTras", "DistDel", "ref","distMedia");grid;
xlabel('Tiempo (ms)');
title('Distancias (cm)');

subplot(3,1,2);
plot(tiempo,tel(:,6), tiempo,tel(:,7));legend('Theta',"Referencia Control �ngulo");grid;
xlabel('Tiempo (ms)');
title('�ngulos');

subplot(3,1,3);
plot( tiempo,tel(:,8));grid;%, tiempo,tel(:,7));legend('Motor Der',"Motor Izq");
title('UReal = fCorr (tanto por uno)');
xlabel('Tiempo (ms)');
end