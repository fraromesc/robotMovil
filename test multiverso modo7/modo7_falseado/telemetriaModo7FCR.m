% Funci�n para mostrar telemetr�a de robot m�vil
% Laboratorio de Rob�tica 4� GIERM 18-19
% Federico Cuesta
% USO: Pasar como par�metro el nombre del archivo de telemetr�a con el
% sigiente contenido por fila:
% -Incremento de tiempo transcurrido desde la lectura anterior (milisegundos),  
% -Distancia medida por sensor Izq/Frontal (cm), 
% -Distancia medida por sensor Dch/Trasero(cm), 
% -Referencia de control (cm), 
% -Modo activo (0: Parado, 1: Control frontal, � 4),
% -Velocidad PWM motor Izq (+/-255, negativo indica marcha atr�s), 
% -Velocidad PWM motor Dch (+/-255, negativo indica marcha atr�s).

function telemetria(archivo)

tel=load('P4G08MODO7.txt');
muestras=length(tel);
disp('Incremento de tiempo m�nimo:'); disp(min(tel(:,1)));
disp('Incremento de tiempo m�ximo:');disp(max(tel(:,1)));
disp('Incremento de tiempo promedio:'); disp(mean(tel(:,1)));
tiempo=zeros(1,muestras);
tiempo(1)=tel(1,1); %Vector de tiempo absoluto
for i=2:muestras
    tiempo(i)=tiempo(i-1)+tel(i,1);
end    

%MODO 7 
subplot(3,1,1);
plot( tiempo,tel(:,2),tiempo,tel(:,3),tiempo,tel(:,4),tiempo,tel(:,5));legend( "wMotorIzda", "wMotorDcha", "refIzda","refDcha");grid;
xlabel('Tiempo (ms)');
ylabel('Velocidades (rpm)');
title('Sensores');

subplot(3,1,2);
plot(tiempo,tel(:,9), tiempo,tel(:,10));legend('PWM_{motor izquierdo}','PWM_{motor derecho}');grid;
xlabel('Tiempo (ms)');
ylabel('Actuaci�n (pwm)');
title('Actuadores');

subplot(3,1,3);
plot(tiempo,tel(:,6), tiempo,tel(:,7),tiempo,tel(:,8));legend('x','y','{phi}');grid;
xlabel('Tiempo (ms)');
ylabel('pose [x,y,{phi}]');
title('Odometr�a');

%%%%%%%%%%
%% CAMBIOS FCR

%Representa trayectoria x,y
figure;
plot(tel(:,6), tel(:,7));

%Hacemos cuenta inversa para intentar determinar los ticks medidos que no
%est�n en telemetr�a
resolIzda = 363.0;      %//Resoluci�n o pulsos por vuelta de la rueda izquierda medidos a mano (dando una vuelta a mano a la rueda dibujando un punto y viendo los ticks)
resolDcha = 362.0;   
for (i=1:100)  %Calculamos los 100 primeros (60 primer tramo). En telemetr�a est�n cada 7 empezando en 17
     pulsosIzda(i)= tel(10+7*i,2)*resolIzda/600;
     pulsosDcha(i)= tel(10+7*i,3)*resolDcha/600;
end     

c = 0.065/2.0;    %//Radio de la rueda del coche [m]
b = 0.15; %CORREGIDA   

%Reestimamos distancias de cuentas 
Lizda = pulsosIzda*(2.0*c*pi/resolIzda);
Ldcha = pulsosDcha*(2.0*c*pi/resolDcha);
Lcentro = (Lizda+Ldcha)/2.0;
  
%Calculamos incremento de Phi con factor de correcci�n por pendiente.
incPhi = (Ldcha-Lizda)/b -0.00302;

%incX = Lcentro*cos(Phi); //cos(incPhi);
%incY = Lcentro*sin(Phi); //sin(incPhi);
PhiT = 0;
Phi=[];
incX=[];
incY=[];
xT=0;
x=[];
yT=0;
y=[];
%Aplicamos odometr�a
for (i=1:length(incPhi))
    PhiT= PhiT+incPhi(i);
    Phi=[Phi; PhiT];
    incX=[incX; Lcentro(i)*cos(Phi(i))];
    xT = xT+incX(i);
    x=[x;xT];
    incY=[incY; Lcentro(i)*sin(Phi(i))];
    yT = yT+incY(i);
    y=[y;yT];
end
figure; plot(pulsosIzda); title('PulsosIzda');
figure; plot(pulsosDcha); title('PulsosDcha');
figure; plot(incPhi); title('incPhi');
figure; plot(Phi); title('Phi'); 
figure; plot(Phi*180/pi); title('Phi Grados'); 
figure; plot(incX); title('incX');
figure; plot(x); title('X');
figure; plot(incY); title('incY');
figure; plot(y); title('Y');