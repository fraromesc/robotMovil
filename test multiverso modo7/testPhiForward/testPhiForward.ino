#include <math.h>                                        // necesaria para utilizar función atan()
#define PI 3.1415926535897932384626433832795             // definición del número PI

//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDel 52
#define echoDel 50

#define trigTras 25
#define echoTras 22

//PINES PARA ENCODERS
#define pinEncoderIzda 20                             //asociada a INT3 => attachInterrupt(3, ..., ...)
#define pinEncoderDcha 21                             //asociada a INT2 => attachInterrupt(2, ..., ...)

  float Lizda=0.0;
  float Ldcha=0.0;
  float Lcentro=0.0;
//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que avanzando hacia izda o dcha a cierta velocidad
void contadorPulsosIzda();                            //Rutina de interrupción para contar número de pulsos de los encoders
void contadorPulsosDcha();                            //Rutina de interrupción para contar número de pulsos de los encoders
void ActualizarOdometria();                           //Funcion que actualiza la odometría del robot
void modo6();                                         //Función para regular la velocidad angular de las ruedas

//PARÁMETROS DEL COCHE
float x = 0.0;                  //Posición en x del vehículo (inicialmente 0)   [m]
float y = 0.0;                  //Posición en y del vehículo (inicialmente 0)   [m] 
float Phi = 0.0;                //Posición en Phi del vehículo (inicialmente 0) [rev]
float x_ant = 0.0;              //Posición en x del vehículo en el instante anterior (inicialmente 0)  [m]
float y_ant = 0.0;              //Posición en y del vehículo en el instante anterior (inicialmente 0)  [m]
float Phi_ant = 0.0;            //Posición en Phi del vehículo en el instante anterior (inicialmente 0)[rev]
float c = 0.065/2.0;            //Radio de la rueda del coche [m]
float b = 0.015;                //Distancia entre las ruedas del coche [m]
float v = 0.0;                  //Velocidad de avance del coche [m/s]
float inc_k = 0.0;              //Incremento de tiempo entre actuaciones
float incX=0.0;                 //Incremento de x calculado por la odometría
float incY=0.0;                 //Incremento de y calculado por la odometría
float incPhi=0.0;               //Incremento de Phi calculado por la odometría

//DISTANCIAS PARA SENSORES ULTRASONIDOS
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

float Tm=0.1;                  //Periodo de muestreo EN SEGUNDOS
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre actuaciones para enviar por telemetría como primer dato
unsigned long t5=0;            //Variable para esperar 1s para contar pulsos de encoder por segundo
unsigned long t6=0;            //Variable para la impresión de telemetría
unsigned long t7=0;            //Variable de tiempo para incrementos de tiempo en la odometría
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

//VARIABLES DE LOS ENCODERS PARA EL CONTROL DE VELOCIDAD DE LOS MOTORES
volatile float pulsosIzda=0.0;      //Contador para pulsos de encoder del motor izquierdo. Se resetea cada 100 ms
volatile float pulsosDcha=0.0;      //Contador para pulsos de encoder del motor derecho. Se resetea cada 100 ms
volatile float pulsosIzdaNoReset = 0.0;
volatile float pulsosIzdaNoResetAnt = 0.0;
volatile float pulsosDchaNoReset = 0.0;
volatile float pulsosDchaNoResetAnt = 0.0;

float wMotorIzda=0.0;               //Velocidad angular del motor izquierdo [rpm]
float wMotorDcha=0.0;               //Velocidad angular del motor derecho [rpm]
float wMotorIzda_ant=0.0;           //Velocidad angular del motor izquierdo en el instante anterior [rpm]
float wMotorDcha_ant=0.0;           //Velocidad angular del motor derecho en el instante anterior [rpm]
float resolucionEncoder=384.0;      //Resolución de los encoders: 8 PPR * 48:1 = 384 [pulsos por revolución del eje de la rueda] 
const float resolIzda = 363.0;      //Resolución o pulsos por vuelta de la rueda izquierda medidos a mano (dando una vuelta a mano a la rueda dibujando un punto y viendo los ticks)
const float resolDcha = 362.0;      //Resolución o pulsos por vuelta de la rueda   derecha medidos a mano (dando una vuelta a mano a la rueda dibujando un punto y viendo los ticks)
bool sentidoGiroMotorIzda=0;        //Sentido de giro del motor izquierdo. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
bool sentidoGiroMotorDcha=0;        //Sentido de giro del motor derecho. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
float estadoEncoder=0.0;            //Estado de la máquina de estados para elegir la referencia del control en bucle abierto y en bucle cerrado
bool bucle=0;                       //Variable que sirve para alternar entre el control en bucle abierto(0) o en bucle cerrado(1)
bool sentGiroIzq = avanza; 
bool sentGiroDer = avanza; 

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT); 
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigTras,OUTPUT);
  pinMode(echoTras,INPUT);
  pinMode(trigDel,OUTPUT);
  pinMode(echoDel,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);

  //INICIALIZACIÓN DE PINES E INTERRUPCIONES DE LOS ENCODERS
  pinMode(pinEncoderIzda,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderIzda),contadorPulsosIzda, CHANGE); //CHANGE); //El pin 3 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 3 cambie de valor

  pinMode(pinEncoderDcha,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderDcha),contadorPulsosDcha, CHANGE); //CHANGE); //El pin 2 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 2 cambie de valor
}

 
void loop() {

  //MEDICIÓN DE VELOCIDAD DE LOS DOS MOTORES  
  if (millis()- t5 >= 100){ //Cada segundo, leer las variables pulsosIzda y pulsosDcha, calcular las velocidades, y resetear estas variables
    ActualizarOdometria();

    wMotorIzda = pulsosIzda*(600.0)/resolIzda;
    wMotorDcha = pulsosDcha*(600.0)/resolDcha;

    if(sentGiroIzq == retrocede) wMotorIzda = -wMotorIzda;
    if(sentGiroDer == retrocede) wMotorDcha = -wMotorDcha;      
    //Actualizar variables
    pulsosIzda = 0.0;
    pulsosDcha = 0.0;
    wMotorIzda_ant = wMotorIzda;
    wMotorDcha_ant = wMotorDcha;
    t5 = millis();
  }
  
  //CONTROL PID DE VELOCIDAD PROPIAMENTE DICHO => CONVENCIÓN: Sentido Giro Antihorario/Izquierda es +; Giro Horario/Derecha es -; 
  //if((millis()-t >= Tm*1000)){                  //Si han pasado Tm segundos y está activo el control en bucle cerrado, lo ejecutamos
    

  //}
  
  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 'm' PARA ALTERNAR): SI NO, DETENER EL COCHE


  //RECEPCIÓN DE CARACTERES
  if (millis()- t2 >= 100){                              //Cada 100 ms, leer el teclado
    if (recepcion =='t'){
      telemetria = !telemetria;                             //Comenzar el experimento enviando los datos de la telemetría
      modo = !modo;                                         //Se alterna el modo (que el control actúe o no)
    }

      if(modo){//&& estadoEncoder < 8){                                     //estadoEncoder == 8 => Detener el coche pues se ha alcanzado la referencia final (200 rpm)
    switch(recepcion){
      case 'w':
        recto(avanza,100);
        break;
      case 's':
        recto(retrocede,100);
        break;
      case 'a':
        pivota(izquierda,100);
        break;
      case 'd':
        pivota(derecha,100);
        break; 
      default:
        recto(avanza,detener);
        break;   
    }
  }
  
    recepcion = 185;                                       //Resetear la tecla en memoria cuando no se esté pulsando
    t2 = millis();
  }

  
  
  //"TELEMETRÍA"
  
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  //Datos del motor izquierdo
  Serial1.print("   x = ");
  Serial1.print(x);
  Serial1.print("   y = ");
  Serial1.print(y);
  Serial1.print("   phi(º) = ");
  Serial1.print(Phi*180.0/PI);
  Serial1.print("   wMotorIzda = ");
  Serial1.print(wMotorIzda);
  Serial1.print("   wMotorDcha = ");
  Serial1.print(wMotorDcha);
  Serial1.print("   pulsosIzdaNoReset = ");
  Serial1.print(pulsosIzdaNoReset);
  Serial1.print("   pulsosDchaNoReset = ");
  Serial1.print(pulsosDchaNoReset);
  Serial1.print("   Lizda(m) = ");
  Serial1.print(Lizda);
  Serial1.print("   Ldcha(m) = ");
  Serial1.print(Ldcha);
  Serial1.println();
 
}                                             //Fin void loop() 
  

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    sentGiroIzq = avanza; 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    sentGiroIzq = retrocede;
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    sentGiroDer = avanza; 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    sentGiroDer = retrocede; 
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 1.05*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}

void contadorPulsosIzda(){              //Rutina de interrupción para contar número de pulsos de los encoders
 pulsosIzdaNoReset++;
 pulsosIzda++;
}
void contadorPulsosDcha(){              //Rutina de interrupción para contar número de pulsos de los encoders
 pulsosDchaNoReset++;
 pulsosDcha++;
}

void ActualizarOdometria(){                           //Funcion que actualiza la odometría del robot

  float deltaPulsosIzda=0.0;
  float deltaPulsosDcha=0.0;
  
  //CALCULAR INCREMENTOS
  //ODOMETRÍA POR VELOCIDADES
  /*
  v = (c/2.0)*(wMotorIzda_ant + wMotorDcha_ant)*(2.0*PI/60.0);      //Velocidad en el instante anterior para calcular la odometría actual, y pasando las velocidades angulares a rad/s
  incPhi = (c/b)*(wMotorDcha_ant-wMotorIzda_ant)*(2.0*PI/60.0)*inc_k; 
  incX = v*(cos(Phi_ant))*inc_k;
  incY = v*(sin(Phi_ant))*inc_k; 
  */

  //ODOMETRÍA POR DISTANCIA RECORRIDA
  // /*  
  Lizda = pulsosIzdaNoReset*2.0*c*PI/resolIzda;
  Ldcha = pulsosDchaNoReset*2.0*c*PI/resolDcha;
  Lcentro = (Lizda+Ldcha)/2.0;
  
  Phi = (Ldcha-Lizda)/b; 
  x= Lcentro*cos(Phi); //cos(incPhi)
  y = Lcentro*sin(Phi); //sin(incPhi) 
  // */

  //CALCULAR ODOMETRÍA CON LOS INCREMENTOS OBTENIDOS
  //x = x_ant + incX;
  //y = y_ant + incY;
  //Phi = Phi_ant + incPhi;
  if(Phi>=360.0*(PI/180.0))Phi-=360.0*(PI/180.0);
  if(Phi<=-360.0*(PI/180.0))Phi+=360.0*(PI/180.0);
  //Phi = atan2(sin(Phi),cos(Phi));    //Mantener Phi entre [-180,180]

  //ACTUALIZACIÓN  
  x_ant = x;
  y_ant = y;
  Phi_ant = Phi;
  inc_k = (millis()-t7)/1000.0;
  t7 = millis();
}
