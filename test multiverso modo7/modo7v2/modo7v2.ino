#include <TimerFive.h>

//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDel 52
#define echoDel 50

#define trigTras 25
#define echoTras 22

//PINES PARA ENCODERS
#define pinEncoderIzda 20                             //asociada a INT3 => attachInterrupt(3, ..., ...)
#define pinEncoderDcha 21                             //asociada a INT2 => attachInterrupt(2, ..., ...)

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwmApp);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwmApp);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwmApp);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwmApp);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwmApp);                   //Función para que avanzando hacia izda o dcha a cierta velocidad
void contadorPulsosIzda();                            //Rutina de interrupción para contar número de pulsos de los encoders
void contadorPulsosDcha();                            //Rutina de interrupción para contar número de pulsos de los encoders
void funcionGiroEncoder(float w);                     //Función para control de velocidad angular del coche, para modos 6, 7 y 8
void TimerInterrupcion();                             //Rutina de interrupción temporizada a 100 ms para almacenar el valor de los ticks  

//PARÁMETROS DEL COCHE
float x = 0.0;                  //Posición en x del vehículo (inicialmente 0)   [m]
float y = 0.0;                  //Posición en y del vehículo (inicialmente 0)   [m] 
float Phi = 0.0;                //Posición en Phi del vehículo (inicialmente 0) [rev]
float x_ant = 0.0;              //Posición en x del vehículo en el instante anterior (inicialmente 0)  [m]
float y_ant = 0.0;              //Posición en y del vehículo en el instante anterior (inicialmente 0)  [m]
float Phi_ant = 0.0;            //Posición en Phi del vehículo en el instante anterior (inicialmente 0)[rev]
float c = 0.065/2.0;            //Radio de la rueda del coche [m]
float b = 0.01;                 //Distancia entre las ruedas del coche [m]
float v = 0.0;                  //Velocidad de avance del coche [m/s]
float inc_k = 0.0;              //Incremento de tiempo entre actuaciones 

//PARÁMETROS DEL CONTROL 
float UeqIzq=15.0;             //Valor de equilibrio de la señal de control del lado izquierdo, necesario para salvar la zona muerta
float UeqDer=15.0;             //Valor de equilibrio de la señal de control del lado derecho, necesario para salvar la zona muerta
float Umin=-255.0;             //Señal de control mínima (para izda y dcha)
float Umax=255.0;              //Señal de control máxima (para izda y dcha)

//CONTROL DE VELOCIDAD DEL MOTOR IZQUIERDO
float UrealIzq=0.0;            //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpIzq=(2.742);           //Cte proporcional para el control del motor izquierdo   
float TiIzq=0.3*2.6;           //Tiempo integral para el control del motor izquierdo
float TdIzq=0.05;              //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0.0;         //Integral del error izquierdo  
float ukIzq=0.0;               //Señal de control instante k para el controlador izquierdo
float ekIzq=0.0;               //Error en k para el controlador izquierdo
float ek1Izq=0.0;              //Error en k-1 para el controlador izquierdo
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior

//CONTROL DE VELOCIDAD DEL MOTOR DERECHO
float UrealDer=0.0;            //Señal de control que se aplica al motor derecho
float Uk_ssDer=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpDer=(3.3670)/1.2;      //Cte proporcional para el control del motor derecho
float TiDer=0.5765*1.5;        //Tiempo integral para el control del motor derecho
float TdDer=0.01;              //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0.0;         //Integral del error derecho  
float ukDer=0.0;               //Señal de control instante k para el controlador derecho
float ekDer=0.0;               //Error en k para el controlador derecho
float ek1Der=0.0;              //Error en k-1 para el controlador derecho
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

//CONTROL DEL AVANCE
float UeqX=0.0;              //Valor de equilibrio de la señal de control del avance
float UminX=-65.0*c;         //Señal de control mínima (para velocidad de avance)
float UmaxX=65.0*c;          //Señal de control máxima (para velocidad de avance)
float UrealX=0.0;            //Señal de control == avance del coche
float Uk_ssX=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpX=100.0;             //Cte proporcional para el control del avance   5    1   
float TiX=0.001;             //Tiempo integral para el control del avance
float TdX=0.1;               //Tiempo derivativo para el control del avance
float Int_err_X=0.0;         //Integral del error del avance 
float ukX=0.0;               //Señal de control instante k para el controlador del avance
float ekX=0.0;               //Error en k para el controlador del avance
float ek1X=0.0;              //Error en k-1 para el controlador del avance
float XDes=1.0;              //Referencia para el avance: distancia a avanzar

//VARIABLES ADICIONALES
float refActual_izda = 0.0;         //Referencia filtrada para los motores de la izda
float refAnt_izda = 0.0;            //Referencia anterior para el filtro de la izda
float alpha_izda = 0.65;            //Alpha para realizar un filtro FIR sobre la referencia de la izda

float refActual_dcha = 0.0;         //Referencia filtrada para los motores de la dcha
float refAnt_dcha = 0.0;            //Referencia anterior para el filtro de la dcha
float alpha_dcha = 0.65;            //Alpha para realizar un filtro FIR sobre la referencia de la dcha

float Tm=0.1;                  //Periodo de muestreo EN SEGUNDOS
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre actuaciones para enviar por telemetría como primer dato
unsigned long t5=0;            //Variable para esperar 1s para contar pulsos de encoder por segundo
unsigned long t6=0;            //Variable para la impresión de telemetría
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

//VARIABLES DE LOS ENCODERS PARA EL CONTROL DE VELOCIDAD DE LOS MOTORES
float pulsosIzda=0.0;           //Contador para pulsos de encoder del motor izquierdo. Se reseteará a 0 cada segundo
float pulsosDcha=0.0;           //Contador para pulsos de encoder del motor derecho. Se reseteará a 0 cada segundo
float pulsosIzdaTemp=0.0;
float pulsosDchaTemp=0.0;
float wMotorIzda=0.0;           //Velocidad angular del motor izquierdo [rpm]
float wMotorDcha=0.0;           //Velocidad angular del motor derecho [rpm]
float wMotorIzda_ant=0.0;       //Velocidad angular del motor izquierdo en el instante anterior [rpm]
float wMotorDcha_ant=0.0;       //Velocidad angular del motor derecho en el instante anterior [rpm]
float resolucionEncoder=384.0;  //Resolución de los encoders: 8 PPR * 48:1 = 384 [pulsos por revolución del eje de la rueda] 
bool sentidoGiroMotorIzda=0;    //Sentido de giro del motor izquierdo. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
bool sentidoGiroMotorDcha=0;    //Sentido de giro del motor derecho. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
int estadoEncoder=0;            //Estado de la máquina de estados para elegir la referencia del control en bucle abierto y en bucle cerrado
float refVelocidad=60.0;        //Referencia de velocidad angular para el bucle cerrado
bool bucle=0;                   //Variable que sirve para alternar entre el control en bucle abierto(0) o en bucle cerrado(1)
bool flagTimer = 0;

bool sentGiroIzq = avanza; 
bool sentGiroDer = avanza; 

void setup(){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); 
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT); 
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigTras,OUTPUT);
  pinMode(echoTras,INPUT);
  pinMode(trigDel,OUTPUT);
  pinMode(echoDel,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth
  Timer5.initialize(100000);
  Timer5.attachInterrupt(TimerInterrupcion);

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);

  //INICIALIZACIÓN DE PINES E INTERRUPCIONES DE LOS ENCODERS

  pinMode(pinEncoderIzda,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderIzda),contadorPulsosIzda,CHANGE);  //El pin 3 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 3 cambie de valor

  pinMode(pinEncoderDcha,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderDcha),contadorPulsosDcha, CHANGE); //El pin 2 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 2 cambie de valor
}

 
void loop() {

  //MEDICIÓN DE VELOCIDAD DE LOS DOS MOTORES
  if (flagTimer == 1){ //Cada segundo, leer las variables pulsosIzda y pulsosDcha, calcular las velocidades, y resetear estas variables
    wMotorIzda = pulsosIzdaTemp*600.0/resolucionEncoder;
    wMotorDcha = pulsosDchaTemp*600.0/resolucionEncoder;

    if(sentGiroIzq == retrocede) wMotorIzda = -wMotorIzda;
    if(sentGiroDer == retrocede) wMotorDcha = -wMotorDcha;      
    //Actualizar variables

    flagTimer = 0;
    //t5 = millis(); 
  }
  
  //CONTROL PID DEL AVANCE Y DE VELOCIDADES PROPIAMENTE DICHO => CONVENCIÓN: Sentido Giro Antihorario/Izquierda es +; Giro Horario/Derecha es -; 
  if((millis()-t >= Tm*1000) ){                  //Si han pasado Tm segundos y está activo el control en bucle cerrado, lo ejecutamos
    
    //Cálculo del error para los dos controladores:
    ekX = XDes - x;

    //Implementación de controladores PID
    ukX = KpX * ekX + (Int_err_X / TiX) + TdX * ((ekX - ek1X) / Tm);  //PID X

    //Saturación del control de avance (sin zona muerta)
    if(ukX>=0)Uk_ssX = UeqX+ukX; //Salvar zona muerta por la zona positiva del control izquierdo sumando señal de equilibrio
    if(ukX<0)Uk_ssX = -UeqX+ukX; //Salvar zona muerta por la zona negativa del control izquierdo restando señal de equilibrio
    //Saturación del controlador X [UminX = -1; UmaxX = 1]
    if (Uk_ssX < UminX)UrealX = UminX;
    else if (Uk_ssX > UmaxX)UrealX = UmaxX;
    else UrealX = Uk_ssX; 
    if(Uk_ssX<Umax && Uk_ssX>Umin)Int_err_X += ekX * Tm; //Anti-windup: sólo para cuando tenemos efecto integral
    
    //Actualizar variables Y ODOMETRÍA
    ek1X = ekX;

    //ODOMETRÍA ---------------
    v = (c/2.0)*(wMotorIzda_ant + wMotorDcha_ant)*(2.0*PI/60.0);      //Velocidad en el instante anterior para calcular la odometría actual, y pasando las velocidades angulares a rad/s
    //if (abs(wMotorDcha-wMotorIzda)>= 2) Phi = Phi_ant + (c/b)*(wMotorDcha_ant-wMotorIzda_ant)*(2.0*PI/60.0)*inc_k*(180.0/PI); 
    //  Phi = Phi_ant + (c/b)*(wMotorDcha_ant-wMotorIzda_ant)*(2.0*PI/60.0)*inc_k*(180.0/PI); 
    Phi = 0.0;
    x = x_ant + v*(cos(Phi_ant*(PI/180.0)))*inc_k;
    y = y_ant + v*(sin(Phi_ant*(PI/180.0)))*inc_k; 
    //FIN ODOMETRÍA -----------
    
    x_ant = x;
    y_ant = y;
    Phi_ant = Phi;
    wMotorIzda_ant = wMotorIzda;
    wMotorDcha_ant = wMotorDcha;
    
    funcionGiroEncoder(0.0);
    t4 = t;
    t = millis();
    inc_k = (t-t4)/1000.0;
  }
    
  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 'm' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo){//&& estadoEncoder < 8){                                     //estadoEncoder == 8 => Detener el coche pues se ha alcanzado la referencia final (200 rpm)
    //Motor izquierdo
    if (UrealIzq > 0) MotorIzquierdo(izquierda,UrealIzq);
    else if (UrealIzq < 0) MotorIzquierdo(derecha,-UrealIzq); 
    else MotorIzquierdo(avanza,detener); 
    //Motor derecho
    if (UrealDer > 0) MotorDerecho(izquierda,UrealDer);
    else if (UrealDer < 0) MotorDerecho(derecha,-UrealDer); 
    else MotorDerecho(avanza,detener); 

    //recto(avanza,100);
    //MotorIzquierdo(avanza,100);
    //MotorDerecho(avanza,100); 
    
  }
  else{
    MotorIzquierdo(avanza,detener);
    MotorDerecho(avanza,detener);
    Int_err_X = 0.0; 
  }

  if (recepcion =='t'){
   telemetria = !telemetria;                             //Comenzar el experimento enviando los datos de la telemetría
   modo = !modo;                                         //Se alterna el modo (que el control actúe o no)
  }
  recepcion = 185;                                       //Resetear la tecla en memoria cuando no se esté pulsando
 
  //"TELEMETRÍA"
  
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  

  Serial1.print("   wMotorIzda = ");
  Serial1.print(wMotorIzda);

  Serial1.print("   wMotorDcha = ");
  Serial1.print(wMotorDcha);


  //Estado del experimento
  Serial1.print("   estadoEncoder = ");
  Serial1.print(estadoEncoder);
  Serial1.print("   refVelocidad = ");
  Serial1.print(refVelocidad);

  //Datos de la odometría
  Serial1.print("   x = ");
  Serial1.print(x);
  Serial1.print("   y = ");
  Serial1.print(y);
  Serial1.print("   phi(º) = ");
  Serial1.print(Phi);
  Serial1.print("   UrealX = ");
  Serial1.print(UrealX);
  Serial1.print("   ekX = ");
  Serial1.print(ekX);
  Serial1.print("   ukX = ");
  Serial1.print(ukX);
  Serial1.print("   w0 = ");
  Serial1.print(UrealX/c);
  Serial1.println();
  /*
  Serial1.print("modo =   ");
  Serial1.print(modo);
  Serial1.print("   pulsosIzda =   ");
  Serial1.print(pulsosIzda);
  Serial1.print("   pulsosDcha =   ");
  Serial1.print(pulsosDcha);
  Serial1.println();
  */



  
 
}                                             //Fin void loop() 
  


//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwmApp){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    sentGiroIzq = avanza; 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    sentGiroIzq = retrocede;
    }
  analogWrite(ENB, pwmApp); 
}
void MotorDerecho(bool sentGiro, int pwmApp)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    sentGiroDer = avanza; 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    sentGiroDer = retrocede; 
    }
  analogWrite(ENA, pwmApp); 
}
void recto(bool sentido,int pwmApp)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwmApp); 
  MotorDerecho(sentido, 0.95*pwmApp);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwmApp) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwmApp*factorDiferencial); 
      MotorDerecho(sentAvance, pwmApp); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwmApp); 
      MotorDerecho(sentAvance, pwmApp*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwmApp)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwmApp); 
      MotorDerecho(retrocede, pwmApp); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwmApp); 
      MotorIzquierdo(retrocede, pwmApp); 
    }
}

void contadorPulsosIzda(){              //Rutina de interrupción para contar número de pulsos de los encoders
    pulsosIzda++;
}
void contadorPulsosDcha(){              //Rutina de interrupción para contar número de pulsos de los encoders
    pulsosDcha++;
}

void funcionGiroEncoder(float VX){
    float w0, wd_des;
    
    //w0 = VX/c;
    w0=60.0;     
    wd_des = w0;
    
    //Filtrado de la referencia
    if (modo){
      refActual_izda = alpha_izda*w0 + (1-alpha_izda)*refAnt_izda;
      refActual_dcha = alpha_dcha*wd_des + (1-alpha_dcha)*refAnt_dcha;
    }
    else{ 
      refActual_izda = 0.0;
      refAnt_izda = 0.0;
      refActual_dcha = 0.0;
      refAnt_dcha = 0.0;
    }

    
    
    //Cálculo del error para los dos controladores:
    ekIzq = refActual_izda - wMotorIzda;
    ekDer = refActual_dcha - wMotorDcha;

    //Implementación de controladores PID
    ukIzq = KpIzq * ekIzq + (Int_err_Izq / TiIzq) + TdIzq * ((ekIzq - ek1Izq) / Tm);  //PID Izquierdo
    ukDer = KpDer * ekDer + (Int_err_Der / TiDer) + TdDer * ((ekDer - ek1Der) / Tm);  //PID Derecho
    
    //Zona muerta y saturación del control izquierdo: LA ZONA MUERTA ES LA MISMA QUE LA DE LOS MODOS 1 AL 4: si no hay un mínimo de Ueq,
    //el motor no puede tener velocidad no nula
    if(ukIzq>=0)Uk_ssIzq = UeqIzq+ukIzq; //Salvar zona muerta por la zona positiva del control izquierdo sumando señal de equilibrio
    if(ukIzq<0)Uk_ssIzq = -UeqIzq+ukIzq; //Salvar zona muerta por la zona negativa del control izquierdo restando señal de equilibrio
    //Saturación del controlador IZQUIERDO [Umin = -180; Umax = 180]
    if (Uk_ssIzq < Umin)UrealIzq = Umin;
    else if (Uk_ssIzq > Umax)UrealIzq = Umax;
    else UrealIzq = Uk_ssIzq; 
    if(Uk_ssIzq<Umax && Uk_ssIzq>Umin)Int_err_Izq += ekIzq * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Zona muerta y saturación del control derecho
    if(ukDer>=0)Uk_ssDer = UeqDer+ukDer; //Salvar zona muerta por la zona positiva del control derecho sumando señal de equilibrio
    if(ukDer<0)Uk_ssDer = -UeqDer+ukDer; //Salvar zona muerta por la zona negativa del control derecho restando señal de equilibrio
    //Saturación del controlador DERECHO [Umin = -180; Umax = 180]
    if (Uk_ssDer < Umin)UrealDer = Umin;
    else if (Uk_ssDer > Umax)UrealDer = Umax;
    else UrealDer = Uk_ssDer; 
    if(Uk_ssDer<Umax && Uk_ssDer>Umin)Int_err_Der += ekDer * Tm; //Anti-windup: sólo para cuando tenemos efecto integral
    
    //Actualizar variables
    ek1Izq = ekIzq;
    ek1Der = ekDer;
    refAnt_izda = refActual_izda;
    refAnt_dcha = refActual_dcha;
}

void TimerInterrupcion(){
  pulsosIzdaTemp = pulsosIzda;
  pulsosDchaTemp = pulsosDcha;
  pulsosIzda = 0.0;
  pulsosDcha = 0.0;
  flagTimer = 1;
}
