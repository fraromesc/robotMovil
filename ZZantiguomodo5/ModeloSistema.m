clc,clear,close all; 


kFinalIzq =mean([1.0614,1.0061,0.9848,0.9987,0.8975,0.8545,1.2249]);
%1.004
tauFinalIzq = mean([500,481.8182,440,500,433.3333,418.1818,438.4615])/1000;
%458.8278
kFinalDer = mean([1.0759,1.077,1.024,1.0179,1.0418,0.9692,0.9851]);
%1.0273
tauFinalDer = mean([622.2222,563.6364,570,515.667,588.8889,536.3636,638.4615])/1000; 
%576.4628
tsBC=0.5;%0.5; 
kc=3/tsBC;
kpIzq=kc*tauFinalIzq/kFinalIzq;
TiIzq=tauFinalIzq;
kpDer=kc*tauFinalDer/kFinalDer;
TiDer=tauFinalDer;

tel=load('exp3.log');

tiempo = tel(:,1);
in  = tel(:,2);
outIzq = tel(:,3);
outDer = tel(:,4);
N=length(in);
incU=[];

figure(); subplot(211); plot(tiempo, in); subplot(212); plot(tiempo, outIzq, tiempo, outDer);
% Obtenci�n de tesc
indiceEscalon=[];
escalon=1; 
Nescalones=1; 
for componente=2:N-1
    if ((abs(in(componente)-in(componente-1))>4) && (in(componente+1)-in(componente)==0)) 
        indiceEscalon(escalon)=componente;
        incU(escalon)=in(componente)-in(componente-1);
        Nescalones=Nescalones+1;
        escalon= escalon+1;
    end
end

escalon=1;
while escalon<Nescalones-1
    %Tomar Yrp
    YrpIzq = mean(outIzq((indiceEscalon(escalon+1)-10):(indiceEscalon(escalon+1)-1)));
    YrpDer = mean(outDer((indiceEscalon(escalon+1)-10):(indiceEscalon(escalon+1)-1)));
    % Tomar Yo
    YoIzq = mean(outIzq((indiceEscalon(escalon)-10):(indiceEscalon(escalon)-1)));
    YoDer = mean(outDer((indiceEscalon(escalon)-10):(indiceEscalon(escalon)-1)));
    % Tomar Ytau
    YtauIzq = (YrpIzq-YoIzq)*0.63+YoIzq;
    YtauDer = (YrpDer-YoDer)*0.63+YoDer;
    %Buscar en el vector de outIzq el momento en que se da yTau, y ese
    %�ndice te lo llevas al vector de tiempos. 

    componente=indiceEscalon(escalon); 
    tTauIzq = 0; 
    while (tTauIzq==0 && componente<indiceEscalon(escalon+1))
       if ( (abs(outIzq(componente-1)-YoIzq)<abs(YtauIzq-YoIzq)) && (abs(outIzq(componente)-YoIzq) >= abs(YtauIzq-YoIzq)))
            tTauIzq= tiempo(componente)   
       end
       componente=componente+1;
       
    end


    componente=indiceEscalon(escalon); 
    tTauDer = 0; 
    while (tTauDer==0 && componente<indiceEscalon(escalon+1))
       if ( (abs(outDer(componente-1)-YoDer)<abs(YtauDer-YoDer)) && (abs(outDer(componente)-YoDer) >= abs(YtauDer-YoDer)))
            tTauDer= tiempo(componente)   
       end
       componente=componente+1;
    end

    tauIzq(escalon)=tTauIzq-tiempo(indiceEscalon(escalon));
    KIzq(escalon)=(YrpIzq-YoIzq)/incU(escalon);


    tauDer(escalon)=tTauDer-tiempo(indiceEscalon(escalon));
    KDer(escalon)=(YrpDer-YoDer)/incU(escalon);
   
    escalon=escalon+1;
end

kMediaIzq=mean(KIzq)
tauMediaIzq=mean(tauIzq)

kMediaDer=mean(KDer)
tauMediaDer=mean(tauDer)
