//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDel 52
#define echoDel 50

#define trigTras 25
#define echoTras 22

//PINES PARA ENCODERS
#define pinEncoderIzda 20                             //asociada a INT3 => attachInterrupt(3, ..., ...)
#define pinEncoderDcha 21                             //asociada a INT2 => attachInterrupt(2, ..., ...)

//ESTADOS OBTENCION CE
#define espera 0
#define estadoPos 1
#define estadoNeg 2
#define procesamiento 5
#define envioDatos 6

int FMS =  espera; 
#define resCE 1
#define N (255/resCE)
#define TAM (2*N+1)

int motIzq[TAM]; 
int motDer[TAM];  
int vel=0; 
int indice=0;


//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que avanzando hacia izda o dcha a cierta velocidad
void contadorPulsosIzda();                  //Rutina de interrupción para contar número de pulsos de los encoders
void contadorPulsosDcha();                  //Rutina de interrupción para contar número de pulsos de los encoders

//PARÁMETROS DEL CONTROL 
float UeqIzq=65.0;             //Valor de equilibrio de la señal de control del lado izquierdo, necesario para salvar la zona muerta
float UeqDer=60.0;             //Valor de equilibrio de la señal de control del lado derecho, necesario para salvar la zona muerta
float Umin=-255.0;             //Señal de control mínima (para izda y dcha)
float Umax=255.0;              //Señal de control máxima (para izda y dcha)

//CONTROL DE VELOCIDAD DEL MOTOR IZQUIERDO
float UrealIzq=0.0;            //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpIzq=1.0;              //Cte proporcional para el control del motor izquierdo   
float TiIzq=0.10;               //Tiempo integral para el control del motor izquierdo
float TdIzq=0.0;               //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0.0;         //Integral del error izquierdo  
float ukIzq=0.0;               //Señal de control instante k para el controlador izquierdo
float ekIzq=0.0;               //Error en k para el controlador izquierdo
float ek1Izq=0.0;              //Error en k-1 para el controlador izquierdo
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior

//CONTROL DE VELOCIDAD DEL MOTOR DERECHO
float UrealDer=0.0;            //Señal de control que se aplica al motor derecho
float Uk_ssDer=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpDer=1.0*0.96;           //Cte proporcional para el control del motor derecho
float TiDer=0.1/0.96;          //Tiempo integral para el control del motor derecho
float TdDer=0.2;               //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0.0;         //Integral del error derecho  
float ukDer=0.0;               //Señal de control instante k para el controlador derecho
float ekDer=0.0;               //Error en k para el controlador derecho
float ek1Der=0.0;              //Error en k-1 para el controlador derecho
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

//VARIABLES ADICIONALES
float Tm=0.01;                 //Periodo de muestreo EN SEGUNDOS
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre lecturas del bluetooth para enviar por telemetría como primer dato
unsigned long t5=0;            //Variable para esperar 1s para contar pulsos de encoder por segundo
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

//VARIABLES DE LOS ENCODERS PARA EL CONTROL DE VELOCIDAD DE LOS MOTORES
float pulsosIzda=0.0;               //Contador para pulsos de encoder del motor izquierdo. Se reseteará a 0 cada segundo
float pulsosDcha=0.0;               //Contador para pulsos de encoder del motor derecho. Se reseteará a 0 cada segundo
float wMotorIzda=0.0;           //Velocidad angular del motor izquierdo [rpm]
float wMotorDcha=0.0;           //Velocidad angular del motor derecho [rpm]
float resolucionEncoder=384.0;      //Resolución de los encoders: 8 PPR * 48:1 = 384 [pulsos por revolución del eje de la rueda] 
bool sentidoGiroMotorIzda=0;    //Sentido de giro del motor izquierdo. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
bool sentidoGiroMotorDcha=0;    //Sentido de giro del motor derecho. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
int estadoEncoder=0;            //Estado de la máquina de estados para elegir la referencia del control en bucle abierto y en bucle cerrado
float refVelocidad=60.0;         //Referencia de velocidad angular para el bucle cerrado
bool bucle=0;                   //Variable que sirve para alternar entre el control en bucle abierto(0) o en bucle cerrado(1)

bool sentGiroIzq = avanza; 
bool sentGiroDer = avanza; 
bool ledb = 1; 
void setup(){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); 
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT); 
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigTras,OUTPUT);
  pinMode(echoTras,INPUT);
  pinMode(trigDel,OUTPUT);
  pinMode(echoDel,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);

  //INICIALIZACIÓN DE PINES E INTERRUPCIONES DE LOS ENCODERS

  pinMode(pinEncoderIzda,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderIzda),contadorPulsosIzda,RISING); //CHANGE); //El pin 3 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 3 cambie de valor

  pinMode(pinEncoderDcha,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderDcha),contadorPulsosDcha, RISING); //CHANGE); //El pin 2 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsos
                                            //Interrumpirá cada vez que el pin 2 cambie de valor
}

 
void loop() {
/*
#define espera 0
#define estadoIzqPos 1
#define estadoIzqNeg 2
#define estadoDerPos 3
#define estadoDerNeg 4
#define procesamiento 5
#define envioDatos 6*/
  
 switch(FMS)
 {
   case espera:
    Serial1.println("A la espera"); 
    if(Serial1.available()>0)
        recepcion = Serial1.read(); 
    if (recepcion == 'm') {
      FMS = estadoPos;
      Serial.println("MotorPositivo"); }
      
    break; 


   case estadoPos:
    pulsosIzda=0;
    pulsosDcha=0; 
    for (int i = 0; i<=N; i+=resCE) 
    {
      MotorIzquierdo(avanza, i); 
      MotorDerecho(avanza, i); 
      pulsosIzda=0;                  
      pulsosDcha=0;
      t=millis(); 
      while(millis()-t<5000);
      motIzq[N+i] =  pulsosIzda*60.0/(5.0*resolucionEncoder);
      motDer[N+i] =  pulsosDcha*60.0/(5.0*resolucionEncoder);
    }
    MotorIzquierdo(avanza, 0); 
    Serial.println("MotorNegativo");   
    FMS = estadoNeg; 
    break; 


   case estadoNeg:
    pulsosIzda=0;
    pulsosDcha=0;       
    for (int i = 0; i<=N; i+=resCE) 
    {
      MotorIzquierdo(retrocede, i);
      MotorDerecho(retrocede, pwm[i]); 
      pulsosIzda=0;                  
      pulsosDcha=0;
      t=millis(); 
      while(millis()-t<5000);
      motIzq[N-i] =  -pulsosIzda*60.0/(5.0*resolucionEncoder);
      motDer[N-i] =  -pulsosDcha*60.0/(5.0*resolucionEncoder);
    }
    MotorIzquierdo(avanza, 0); 
    Serial.println("EnvioDatos");   
    FMS = envioDatos; 
    break;


   case envioDatos: 
    Serial1.println("MotorIzq"); 
    for (int i = 0; i<TAM; i++  )
    {
      Serial1.print(i); 
      Serial1.print(","); 
      Serial1.print(motIzq[i]); 
      Serial1.println(";");
    }
    Serial1.println("MotorDer"); 
    for (int i = 0; i<TAM; i++  )
    {
      Serial1.print(i); 
      Serial1.print(","); 
      Serial1.print(motDer[i]); 
      Serial1.println(";");
    }

    Serial1.println("FIN TRANSMISION");
    FMS =espera;


 }


     
  
}

 

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    sentGiroIzq = avanza; 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    sentGiroIzq = retrocede;
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    sentGiroDer = avanza; 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    sentGiroDer = retrocede; 
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.902*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}

void contadorPulsosIzda(){              //Rutina de interrupción para contar número de pulsos de los encoders
    pulsosIzda++;
}
void contadorPulsosDcha(){              //Rutina de interrupción para contar número de pulsos de los encoders
    pulsosDcha++;
}
