    
function testCE()

motDer=load("motDer.txt");
motIzq=load("motIzq.txt");
N=length(motDer);

motDer(:,1)=motDer(:,1)-((N-1)/2) 
motIzq(:,1)=motIzq(:,1)-((N-1)/2) 


figure(1); 
plot(motDer(:,1),motDer(:,2), motIzq(:,1),motIzq(:,2)); 
grid on; xlabel("pwm"); ylabel("velocidad [rpm]"); legend('Motor Derecho', 'Motor Izquierdo','location','best');
title('Característica Estática de los Motores');

end