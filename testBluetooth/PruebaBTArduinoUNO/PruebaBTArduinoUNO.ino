

//PUTTY => TERMINAL => LOCAL ECHO = FORCE ON; LINE ENDING = FORCE ON;
//PUTTY => SESSION => LOGGING => ACTIVAR "PRINTABLE OUTPUT" BROWSE => CAMBIAR UBICACIÓN PARA GUARDAR EL .log
//Si es la primera vez que conectamos el módulo por BT al PC, hay que cambiarle la velocidad en el administrador de dispositivos a:
//Datos Módulo:
//Velocidad = 38400 baudios
//Contraseña = 1234

#include <SoftwareSerial.h>
SoftwareSerial mySerial(10,11); //Rx,Tx    //PARA ARDUINO MEGA cambiar "mySerial" por Serial2 y conectar al puerto serie 2 del Mega

unsigned char recepcion = 'a';

void setup() {
  Serial.begin(9600);
  mySerial.begin(38400);
}

void loop() {

  //Escribir en el Putty por arduino
  mySerial.println("Hola me llamo Paco");
  
  //Leer Putty por el arduino
  /*
   if(mySerial.available()>0){
        recepcion = mySerial.read();      
  }
  
  Serial.println(recepcion);
 */ 
}
