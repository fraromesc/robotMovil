//Cargar este programa en un arduino uno
//se debe desenchufar la alimentación del módulo, cargar el programa, y alimentar el módulo con el botón 
//que integra el módulo pulsado. Este hecho, junto con el hecho de configurar la velocidad de la comunicación a 38400 baudios, 
//hace que el módulo entre en el llamado modo AT o modo de configuración.

//Comandos AT para configuración y consulta: https://www.prometec.net/configurando-bluetooth-hc-05-y-hc-06/

#include <SoftwareSerial.h>
SoftwareSerial mySerial(10,11); //Rx,Tx

void setup() {
  Serial.begin(9600);
  mySerial.begin(38400);
  while(!Serial){
    ; //wait for serial port to connect
  }
  Serial.println("Listo");
}

void loop() {
  // put your main code here, to run repeatedly:
  if(mySerial.available()){
    Serial.write(mySerial.read());
  }
  if(Serial.available()){
    mySerial.write(Serial.read());
  }
}
