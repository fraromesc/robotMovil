//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDer 52
#define echoDer 50

#define trigIzq 25
#define echoIzq 22

//CABECERAS DE FUNCIONES
float distanciaUS(int TriggerPin, int EchoPin);       //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que  avanzando hacia izda o dcha a cierta velocidad

//PARÁMETROS DEL CONTROL 
float UeqIzq=65.0;             //Valor de equilibrio de la señal de control del lado izquierdo, necesario para salvar la zona muerta
float UeqDer=60.0;             //Valor de equilibrio de la señal de control del lado derecho, necesario para salvar la zona muerta
float Umin=-180.0;             //Señal de control mínima (para izda y dcha)
float Umax=180.0;              //Señal de control máxima (para izda y dcha)

//CONTROL IZQUIERDO
float UrealIzq=0.0;            //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpIzq=30.0;              //Cte proporcional para el control del motor izquierdo   
float TiIzq=1.0;               //Tiempo integral para el control del motor izquierdo
float TdIzq=0.2;               //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0.0;         //Integral del error izquierdo  
float ukIzq=0.0;               //Señal de control instante k para el controlador izquierdo
float ekIzq=0.0;               //Error en k para el controlador izquierdo
float ek1Izq=0.0;              //Error en k-1 para el controlador izquierdo
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior

//CONTROL DERECHO
float UrealDer=0.0;            //Señal de control que se aplica al motor derecho
float Uk_ssDer=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpDer=30*0.96;           //Cte proporcional para el control del motor derecho
float TiDer=1.0/0.96;          //Tiempo integral para el control del motor derecho
float TdDer=0.2;               //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0.0;         //Integral del error derecho  
float ukDer=0.0;               //Señal de control instante k para el controlador derecho
float ekDer=0.0;               //Error en k para el controlador derecho
float ek1Der=0.0;              //Error en k-1 para el controlador derecho
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

float Tm=0.01;                 //Periodo de muestreo EN SEGUNDOS
float Int_err=0.0;             //Integral del error
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre lecturas del bluetooth para enviar por telemetría como primer dato
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
int estado = 0;                //Estado de la máquina de estados para elegir la referencia del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

void setup(){
  //PINES DEL L298N
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigIzq,OUTPUT);
  pinMode(echoIzq,INPUT);
  pinMode(trigDer,OUTPUT);
  pinMode(echoDer,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    // Comunicación por puerto serie arduino
  Serial1.begin(38400);  // Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);
}
 
void loop() {

  //CONTROLADORES
  if (millis()- t >= Tm*1000)                         //Esperar Tm (millis-t == tiempo desde que se ejecutó el control la última vez EN MILISEGUNDOS)
  {                                                   //Tm * 1000 = pasar Tm A MILISEGUNDOS

    //Medida de los ultrasonidos
    distIzq=distanciaUS(trigIzq, echoIzq);
    distDer=distanciaUS(trigDer, echoDer);

    //Filtro de medidas
    if(millis()>1000){                                //No filtramos las primeras medidas, filtramos cuando ya se han actualizado varias veces 
                                                      //las medidas del instante anterior
      if((distIzq-distIzqAnt)>30)distIzq=0.1*distIzq+distIzqAnt;
      if((distDer-distDerAnt)>30)distDer=0.1*distDer+distDerAnt;
    }
  
    //Cálculo de errores
    ekIzq=distIzq-ref;                                //Cálculo del error para el controlador izquierdo (control separado de las dos ruedas)      
    ekDer=distDer-ref;                                //Cálculo del error para el controlador derecho (control separado de las dos ruedas)
  
    //Implementación de controladores PID
    ukIzq = KpIzq * ekIzq + (Int_err_Izq / TiIzq) + TdIzq * ((ekIzq - ek1Izq) / Tm);  //PID Izquierdo
    ukDer = KpDer * ekDer + (Int_err_Der / TiDer) + TdDer * ((ekDer - ek1Der) / Tm);  //PID Derecho

    //Zona muerta y saturación del control izquierdo
    if(ukIzq>=0)Uk_ssIzq = UeqIzq+ukIzq; //Salvar zona muerta por la zona positiva del control izquierdo sumando señal de equilibrio
    if(ukIzq<0)Uk_ssIzq = -UeqIzq+ukIzq; //Salvar zona muerta por la zona negativa del control izquierdo restando señal de equilibrio
    //Saturación del controlador IZQUIERDO [Umin = -180; Umax = 180]
    if (Uk_ssIzq < Umin)UrealIzq = Umin;
    else if (Uk_ssIzq > Umax)UrealIzq = Umax;
    else UrealIzq = Uk_ssIzq; 
    if(Uk_ssIzq<Umax && Uk_ssIzq>Umin)Int_err_Izq += ekIzq * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Zona muerta y saturación del control derecho
    if(ukDer>=0)Uk_ssDer = UeqDer+ukDer; //Salvar zona muerta por la zona positiva del control derecho sumando señal de equilibrio
    if(ukDer<0)Uk_ssDer = -UeqDer+ukDer; //Salvar zona muerta por la zona negativa del control derecho restando señal de equilibrio
    //Saturación del controlador DERECHO [Umin = -180; Umax = 180]
    if (Uk_ssDer < Umin)UrealDer = Umin;
    else if (Uk_ssDer > Umax)UrealDer = Umax;
    else UrealDer = Uk_ssDer; 
    if(Uk_ssDer<Umax && Uk_ssDer>Umin)Int_err_Der += ekDer * Tm; //Anti-windup: sólo para cuando tenemos efecto integral
    
    //Actualizar variables
    ek1Izq = ekIzq;
    ek1Der = ekDer;
    t = millis();

    distIzqAnt = distIzq;
    distDerAnt = distDer;

    //Esperar en régimen permanente
    if(!((ekIzq <= 1.20 && ekIzq >= -1.20)&&(ekDer <= 1.20 && ekDer >= -1.20))) t3 = millis();  //Si el error NO es casi cero (banda de error permitida de 1.2 cm), 
                                                                                                //actualizar t3 a millis, si sí lo es, comenzar la espera de régimen permanente 
    //Máquina de estados para decidir la referencia     //Idea: repetir TEST transparencias para el MODO 1 y 2
    if(((millis()-t3)>=2500)&& estado < 2 )estado++;    //Si se alcanza la referencia durante 3 segundos al menos, cambiar de referencia
    switch (estado){
      case 0:                                           //Estado == 0 => Referencia = 30 cm
        ref=30;
        break; 
      case 1:                                           //Estado == 1 => Refererncia = 50 cm 
        ref=50; 
        break;  
      default:                                          //Estado == 2 => Detener el coche pues se ha alcanzado la referencia final (da igual la referencia)
        break; 
    }
  }

  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 'm' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo && estado < 2){                              //Estado == 2 => Detener el coche pues se ha alcanzado la referencia final (50 cm)
    //Motor izquierdo
    if (UrealIzq > 0) MotorIzquierdo(avanza,UrealIzq);
    else if (UrealIzq < 0) MotorIzquierdo(retrocede,-UrealIzq); 
    else MotorIzquierdo(avanza,detener); 
    //Motor derecho
    if (UrealDer > 0) MotorDerecho(avanza,0.902*UrealDer);
    else if (UrealDer < 0) MotorDerecho(retrocede,-UrealDer*0.902); 
    else MotorDerecho(avanza,detener); 
  }
  else{
    MotorIzquierdo(avanza,detener);
    MotorDerecho(avanza,detener); 
  }


//AJUSTE EN LÍNEA DE LAS CONSTANTES DEL CONTROL
  if (millis()- t2 >= 100){                              //Cada 100 ms, leer el teclado
    if (recepcion == 'l') ladoAjuste = !ladoAjuste;      //Con cada pulsación de l, se alternan los controles que podemos cambiar en línea (izda - dcha)
    else if (recepcion == 'z') estado = 0;               //Esta tecla resetea el estado sin usar el botón de reset del arduino
    else if (recepcion =='1') ajuste*=10;
    else if (recepcion =='2') ajuste*=0.1; 
    else if (recepcion =='x'){                           //Resetear integrales del error
      Int_err_Izq = 0;
      Int_err_Der = 0;
    }
    else if (recepcion =='t'){
      telemetria = !telemetria; //Comenzar el experimento enviando los datos de la telemetría
      modo = !modo;             //Se alterna el modo (que el control actúe o no)
    }
    else if (ladoAjuste == izquierda){
    if (recepcion== 'q') KpIzq+=ajuste;
    else if (recepcion=='a') KpIzq-=ajuste;
    else if (recepcion =='w') TiIzq+=ajuste;
    else if (recepcion =='s') TiIzq-=ajuste;
    else if (recepcion =='e') TdIzq+=ajuste;
    else if (recepcion =='d') TdIzq-=ajuste;
    else if (recepcion =='r') UeqIzq+=ajuste;
    else if (recepcion =='f') UeqIzq-=ajuste;}
    else if (ladoAjuste == derecha){
    if (recepcion== 'q') KpDer+=ajuste;
    else if (recepcion=='a') KpDer-=ajuste;
    else if (recepcion =='w') TiDer+=ajuste;
    else if (recepcion =='s') TiDer-=ajuste;
    else if (recepcion =='e') TdDer+=ajuste;
    else if (recepcion =='d') TdDer-=ajuste;
    else if (recepcion =='r') UeqDer+=ajuste;
    else if (recepcion =='f') UeqDer-=ajuste;}
   
   recepcion = 185;                                   //Resetear la tecla en memoria cuando no se esté pulsando
   t2 = millis();
  }
 
  //"TELEMETRÍA"
  /* 
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  Serial1.print("USIzdo = ");
  Serial1.print(distIzq);
  Serial1.print("   USDcho = ");
  Serial1.print(distDer);
  Serial1.print("   ajuste = ");
  Serial1.print(ajuste);
  Serial1.print("   estado = ");
  Serial1.print(estado);
  Serial1.print("   Ref = ");
  Serial1.print(ref);

  if(ladoAjuste == izquierda){                     //Ajustamos sólo las constantes del control izquierdo
    Serial1.print("   UrealIzq = ");
    Serial1.print(UrealIzq);
    Serial1.print("   KpIzq = ");
    Serial1.print(KpIzq);
    Serial1.print("   TiIzq = ");
    Serial1.print(TiIzq);
    Serial1.print("   TdIzq = ");
    Serial1.print(TdIzq);
    Serial1.print("   UeqIzq = ");
    Serial1.print(UeqIzq);
  }
  else{                                            //Ajustamos sólo las constantes del control izquierdo
    Serial1.print("   UrealDer = ");
    Serial1.print(UrealDer);
    Serial1.print("   KpDer = ");
    Serial1.print(KpDer);
    Serial1.print("   TiDer = ");
    Serial1.print(TiDer);
    Serial1.print("   TdDer = ");
    Serial1.print(TdDer);
    Serial1.print("   UeqDer = ");
    Serial1.print(UeqDer);
  }
  Serial1.println();
  */

    //Serial.print("   KpDer = ");
    //Serial.print(KpDer);
    //Serial.print("   TiDer = ");
    //Serial.print(TiDer);
    //Serial.print("   TdDer = ");
    //Serial.print(TdDer);

  //TELEMETRÍA
   
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  //Escribir en Putty
  if(telemetria){
  Serial1.print(millis()-t4);                  //dat1: tiempo entre lecturas de ultrasonidos
  Serial1.print(",");
  //Sensores
  Serial1.print(distIzq);                      //dat2: lectura del sensor izquierdo
  Serial1.print(",");
  Serial1.print(distDer);                      //dat3: lectura del sensor derecho
  Serial1.print(",");
  Serial1.print(ref);                          //dat4: referencia
  Serial1.print(",");
  
  //Actuadores
  Serial1.print(UrealIzq);                     //dat5: escritura del actuador izquierdo
  Serial1.print(",");
  Serial1.print(UrealDer);                     //dat6: escritura del actuador derecho
  Serial1.print(",");
  Serial1.print(millis());                     //dat7: milisegundos desde que arrancó el programa, para comparar con las marcas temporales de Tm
  Serial1.print(";");
  Serial1.println();
  }

  t4 = millis();                               //Actualizar t4 para el cálculo del tiempo entre lecturas del bluetooth

}

//DEFINICIÓN DE LAS FUNCIONES
float distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);                //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);               //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);            //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;          //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){    //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)        //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.960*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}
