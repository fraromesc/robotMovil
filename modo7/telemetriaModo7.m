% Funci�n para mostrar telemetr�a de robot m�vil
% Laboratorio de Rob�tica 4� GIERM 18-19
% Federico Cuesta
% USO: Pasar como par�metro el nombre del archivo de telemetr�a con el
% sigiente contenido por fila:
% -Incremento de tiempo transcurrido desde la lectura anterior (milisegundos),  
% -Distancia medida por sensor Izq/Frontal (cm), 
% -Distancia medida por sensor Dch/Trasero(cm), 
% -Referencia de control (cm), 
% -Modo activo (0: Parado, 1: Control frontal, � 4),
% -Velocidad PWM motor Izq (+/-255, negativo indica marcha atr�s), 
% -Velocidad PWM motor Dch (+/-255, negativo indica marcha atr�s).

function telemetria(archivo)

pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470; % Definici�n de pi

tel=load('P4G08MODO7.txt');
muestras=length(tel);
disp('Incremento de tiempo m�nimo:'); disp(min(tel(:,1)));
disp('Incremento de tiempo m�ximo:');disp(max(tel(:,1)));
disp('Incremento de tiempo promedio:'); disp(mean(tel(:,1)));
tiempo=zeros(1,muestras);
tiempo(1)=tel(1,1); %Vector de tiempo absoluto
for i=2:muestras
    tiempo(i)=tiempo(i-1)+tel(i,1);
end 

%CALCULAR INCREMENTOS DE PULSOS A PARTIR DE LOS PULSOS
pulsosIzda=tel(:,12); 
for i=2:length(pulsosIzda)
    incPulsosIzda(i,1) = pulsosIzda(i)-pulsosIzda(i-1); 
end
pulsosIzda(1)=0;

pulsosDcha=tel(:,13); 
for i=2:length(pulsosDcha)
    incPulsosDcha(i,1) = pulsosDcha(i)-pulsosDcha(i-1); 
end
pulsosDcha(1)=0; 

%MODO 7
figure(1);            % Informaci�n de sensores y actuadores
set(gcf,'color','w'); % fondo blanco
subplot(2,1,1);
plot( tiempo,tel(:,2),tiempo,tel(:,3),tiempo,tel(:,4),tiempo,tel(:,5));legend( "wMotorIzda", "wMotorDcha", "refIzda","refDcha");grid;
xlabel('Tiempo (ms)');
ylabel('Velocidades (rpm)');
title('Sensores');

subplot(2,1,2);
plot(tiempo,tel(:,14), tiempo,tel(:,15));legend('PWM_{motor izquierdo}','PWM_{motor derecho}');grid;
xlabel('Tiempo (ms)');
ylabel('Actuaci�n (pwm)');
title('Actuadores');


figure(2);            % Informaci�n de la odometr�a
set(gcf,'color','w'); % fondo blanco
subplot(3,1,1);
plot(tiempo,tel(:,6), tiempo,tel(:,7),tiempo,tel(:,8)*(180/pi));legend('x(m)','y(m)','phi(�)');grid;
xlabel('Tiempo (ms)');
ylabel('pose del robot [x,y,{phi}]');
title('Odometr�a');

subplot(3,1,2);
plot(tiempo,tel(:,9), tiempo,tel(:,10),tiempo,tel(:,11)*(180/pi));legend('incX(m)','incY(m)','incPhi(�)');grid;
xlabel('Tiempo (ms)');
ylabel('incrementos de la pose [incX,incY,incPhi]');

subplot(3,1,3)
%plot(tiempo,tel(:,12), tiempo,tel(:,13));legend('pulsosIzda','pulsosDcha');grid;
plot(tiempo,incPulsosIzda, tiempo,incPulsosDcha);legend('pulsosIzda','pulsosDcha');grid;    %Calculados los incrementos de pulsos
title('Pulsos de los encoders');
xlabel('Tiempo (ms)');
ylabel('Ticks');


figure(3);            % Trayectoria realizada y pulsos de encoder 
set(gcf,'color','w'); % fondo blanco
plot(tel(:,6),tel(:,7), 'LineWidth', 2);grid on;hold on;
xlabel('x(m)');
ylabel('y(m)');
title('Trayectoria seguida por el robot');

x1=0;x2=1;y1=0;y2=1;
x = [x1, x2, x2, x1, x1];
y = [y1, y1, y2, y2, y1];
plot(x, y, 'r-');legend('recorrido real','recorrido deseado','location','best');
hold on;



end