#include <math.h>

//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDel 52
#define echoDel 50

#define trigTras 25
#define echoTras 22

//PINES PARA ENCODERS
#define pinEncoderIzda 20                             //asociada a INT3 => attachInterrupt(3, ..., ...)
#define pinEncoderDcha 21                             //asociada a INT2 => attachInterrupt(2, ..., ...)


//ESTADOS PARA LA MAQUINA DE ESTADOS Y SUS VARIABLES
#define INICIO             0
#define RECEPCION_DESTINO  1
#define COMPROBACION_PHI_X 2
#define CORRECCION_PHI_X   3             
#define DESPLAZAMIENTO_X   4
#define COMPROBACION_PHI_Y 5
#define CORRECCION_PHI_Y   6
#define DESPLAZAMIENTO_Y   7

char *fsm[]={                                        //Se utiliza en telemetría de depuración
"INICIO",  
"RECEPCION_DESTINO",
"COMPROBACION_PHI_X",
"CORRECCION_PHI_X",
"DESPLAZAMIENTO_X",
"COMPROBACION_PHI_Y",
"CORRECCION_PHI_Y",
"DESPLAZAMIENTO_Y",  
};
int FSM = INICIO;
bool destino = 0;                                    //Booleano para mantenernos en el estado RECEPCION_DESTINO hasta que nos den un destino por telemetría
int xDesInt = 0;                                     //Posición x destino recibida por Putty
int yDesInt = 0;                                     //Posición y destino recibida por Putty 
float xDes = 0.0;
float yDes = 0.0;

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que avanzando hacia izda o dcha a cierta velocidad
void contadorPulsosIzda();                            //Rutina de interrupción para contar número de pulsos de los encoders
void contadorPulsosDcha();                            //Rutina de interrupción para contar número de pulsos de los encoders
void ActualizarOdometria();                           //Funcion que actualiza la odometría del robot
void modo6();                                         //Función para regular la velocidad angular de las ruedas 
void controlPhi();                                    //Función para control de ángulo de la pose en el modo 8

//PARÁMETROS DEL COCHE
float x = 0.0;                  //Posición en x del vehículo (inicialmente 0)   [m]
float y = 0.0;                  //Posición en y del vehículo (inicialmente 0)   [m] 
float Phi = 0.0;                //Posición en Phi del vehículo (inicialmente 0) [rad]
float x_ant = 0.0;              //Posición en x del vehículo en el instante anterior (inicialmente 0)  [m]
float y_ant = 0.0;              //Posición en y del vehículo en el instante anterior (inicialmente 0)  [m]
float Phi_ant = 0.0;            //Posición en Phi del vehículo en el instante anterior (inicialmente 0)[rad]
float c = 0.065/2.0;            //Radio de la rueda del coche [m]
float b = 0.15;                 //Distancia entre las ruedas del coche [m]
float v = 0.0;                  //Velocidad de avance del coche [m/s]
float inc_k = 0.0;              //Incremento de tiempo entre actuaciones
float incX=0.0;                 //Incremento de x calculado por la odometría
float incY=0.0;                 //Incremento de y calculado por la odometría
float incPhi=0.0;               //Incremento de Phi calculado por la odometría

//PARÁMETROS DEL CONTROL 
float UeqIzq=65.0;             //Valor de equilibrio de la señal de control del lado izquierdo, necesario para salvar la zona muerta
float UeqDer=60.0;             //Valor de equilibrio de la señal de control del lado derecho, necesario para salvar la zona muerta
float Umin=-255.0;             //Señal de control mínima (para izda y dcha)
float Umax=255.0;              //Señal de control máxima (para izda y dcha)
float UeqPhi=0.0;              //Valor de equilibrio para phi
float UminPhi=-35.0;           //Señal de control mínima (para phi)
float UmaxPhi=35.0;            //Señal de control máxima (para phi)
bool resetErrorMot = 1;        //Bandera para reiniciar error al empezar el  controlador

//CONTROL DE VELOCIDAD DEL MOTOR IZQUIERDO
float UrealIzq=0.0;            //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpIzq=(2.742);           //Cte proporcional para el control del motor izquierdo   
float TiIzq=0.3*2.30;          //Tiempo integral para el control del motor izquierdo
float TdIzq=0.05;              //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0.0;         //Integral del error izquierdo  
float ukIzq=0.0;               //Señal de control instante k para el controlador izquierdo
float ekIzq=0.0;               //Error en k para el controlador izquierdo
float ek1Izq=0.0;              //Error en k-1 para el controlador izquierdo
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior

//CONTROL DE VELOCIDAD DEL MOTOR DERECHO
float UrealDer=0.0;            //Señal de control que se aplica al motor derecho
float Uk_ssDer=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpDer=(3.3670)/1.2;      //Cte proporcional para el control del motor derecho
float TiDer=0.5765*1.5;        //Tiempo integral para el control del motor derecho
float TdDer=0.01;              //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0.0;         //Integral del error derecho  
float ukDer=0.0;               //Señal de control instante k para el controlador derecho
float ekDer=0.0;               //Error en k para el controlador derecho
float ek1Der=0.0;              //Error en k-1 para el controlador derecho
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

//CONTROL DE PHI
float UrealPhi=0.0;            //Señal de control que se aplica a Phi
float Uk_ssPhi=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpPhi=150.0;             //Cte proporcional para el control Phi
float TiPhi=0.01;              //Tiempo integral para el control Phi
float TdPhi=0.001;             //Tiempo derivativo para el control Phi
float Int_err_Phi=0.0;         //Integral del error Phi  
float ukPhi=0.0;               //Señal de control instante k para el controlador Phi
float ekPhi=0.0;               //Error en k para el controlador Phi
float ek1Phi=0.0;              //Error en k-1 para el controlador Phi
bool resetErrorPhi = 1;        //Bandera para reiniciar error al empezar el  controlador

//VARIABLES ADICIONALES
float refActualIzda = 0.0;     //Referencia filtrada para el motor izquierdo
float refAntIzda = 0.0;        //Referencia anterior para el filtro del motor izquierdo
float refActualDcha = 0.0;     //Referencia filtrada para el motor derecho
float refAntDcha = 0.0;        //Referencia anterior para el filtro del motor derecho
float refPhi=0.0*(PI/180.0);   //Referencia para controlPhi
float alpha = 0.65;            //Alpha para realizar un filtro EMA sobre la referencia  
float alphaPhi = 0.4;          //Alpha para realizar un filtro EMA sobre la referencia del control del angulo
float refActualPhi = 0.0;      //Referencia filtrada para el control del angulo
float refAntPhi = 0.0;         //Referencia anterior para el filtro del control del angulo
float Tm=0.1;                  //Periodo de muestreo EN SEGUNDOS
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre actuaciones para enviar por telemetría como primer dato
unsigned long t5=0;            //Variable para esperar 1s para contar pulsos de encoder por segundo
unsigned long t6=0;            //Variable para la impresión de telemetría
unsigned long t7=0;            //Variable de tiempo para incrementos de tiempo en la odometría
unsigned long t8=0;            //Tiempo necesario en régimen permanente para considerar que hemos alcanzado la referencia en los controles de ángulo y distancia
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

//VARIABLES DE LOS ENCODERS PARA EL CONTROL DE VELOCIDAD DE LOS MOTORES
volatile float pulsosIzda=0.0;      //Contador para pulsos de encoder del motor izquierdo
volatile float pulsosDcha=0.0;      //Contador para pulsos de encoder del motor derecho
float pulsosIzda_odom=0.0;          //Contador para pulsos de encoder del motor izquierdo para la odometría. Se actualiza cada 100 ms
float pulsosIzda_w=0.0;             //Contador para pulsos de encoder del motor izquierdo para la w. Se actualiza cada 100 ms
float pulsosDcha_odom=0.0;          //Contador para pulsos de encoder del motor derecho para la odometría. Se actualiza cada 100 ms
float pulsosDcha_w=0.0;             //Contador para pulsos de encoder del motor derecho para la w. Se actualiza cada 100 ms
float pulsosIzda_ant_odom=0.0;      //Contador para pulsos de encoder del motor izquierdo del instante anterior para la odometría. Se actualiza cada 100 ms
float pulsosIzda_ant_w=0.0;         //Contador para pulsos de encoder del motor izquierdo del instante anterior para la w. Se actualiza cada 100 ms
float pulsosDcha_ant_odom=0.0;      //Contador para pulsos de encoder del motor derecho del instante anterior para la odometría. Se actualiza cada 100 ms
float pulsosDcha_ant_w=0.0;         //Contador para pulsos de encoder del motor derecho del instante anterior para la w. Se actualiza cada 100 ms
float wMotorIzda=0.0;               //Velocidad angular del motor izquierdo [rpm]
float wMotorDcha=0.0;               //Velocidad angular del motor derecho [rpm]
float wMotorIzda_ant=0.0;           //Velocidad angular del motor izquierdo en el instante anterior [rpm]
float wMotorDcha_ant=0.0;           //Velocidad angular del motor derecho en el instante anterior [rpm]
float resolucionEncoder=384.0;      //Resolución de los encoders: 8 PPR * 48:1 = 384 [pulsos por revolución del eje de la rueda] 
const float resolIzdaPhi = 245.0;   //Resolución o pulsos por vuelta de la rueda izquierda medidos a mano (dando una vuelta a mano a la rueda dibujando un punto y viendo los ticks)
const float resolDchaPhi = 247.0;   //Resolución o pulsos por vuelta de la rueda   derecha medidos a mano (dando una vuelta a mano a la rueda dibujando un punto y viendo los ticks)
const float resolIzda = 363.0;      //Resolución de la rueda izquierda calculada manualmente
const float resolDcha = 362.0;      //Resolución de la rueda derecha calculada manualmente
bool sentidoGiroMotorIzda=0;        //Sentido de giro del motor izquierdo. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
bool sentidoGiroMotorDcha=0;        //Sentido de giro del motor derecho. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
float estadoEncoder=0.0;            //Estado de la máquina de estados para elegir la referencia del control en bucle abierto y en bucle cerrado
float refVelocidadIzda=50.0;        //Referencia de velocidad angular para la rueda izquierda en el bucle cerrado
float refVelocidadDcha=50.0;        //Referencia de velocidad angular para la rueda derecha en el bucle cerrado
bool bucle=0;                       //Variable que sirve para alternar entre el control en bucle abierto(0) o en bucle cerrado(1)
bool sentGiroIzq = avanza;          //Sentido de giro de la rueda izquierda
bool sentGiroDer = avanza;          //Sentido de giro de la rueda derecha

//BUFFER PARA RECEPCIÓN POR PUTTY
char bufferRecepcion[20]="";        //Cadena recibida por putty
int i;                              //Índice para recorrer la cadena

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT); 
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigTras,OUTPUT);
  pinMode(echoTras,INPUT);
  pinMode(trigDel,OUTPUT);
  pinMode(echoDel,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);

  //INICIALIZACIÓN DE PINES E INTERRUPCIONES DE LOS ENCODERS
  pinMode(pinEncoderIzda,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderIzda),contadorPulsosIzda, CHANGE); //CHANGE); //El pin 3 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsosIzda
                                            //Interrumpirá cada vez que el pin 3 cambie de valor

  pinMode(pinEncoderDcha,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderDcha),contadorPulsosDcha, CHANGE); //CHANGE); //El pin 2 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsosDcha
                                            //Interrumpirá cada vez que el pin 2 cambie de valor
}

 
void loop() {

  //MEDICIÓN DE VELOCIDAD DE LOS DOS MOTORES  
  if (millis()-t5>=100){ //Cada 100 milisegundos, leer las variables pulsosIzda y pulsosDcha, calcular las velocidades, y resetear estas variables
    ActualizarOdometria();

    pulsosIzda_w = pulsosIzda;
    pulsosDcha_w = pulsosDcha;

    wMotorIzda = (pulsosIzda_w-pulsosIzda_ant_w)*(600.0)/(2.0*resolIzda);  //El controlador se diseñó para interrupciones tipo RISING, hemos cambiado a CHANGE para odometría más precisa, de ahí el dividido por 2.0
    wMotorDcha = (pulsosDcha_w-pulsosDcha_ant_w)*(600.0)/(2.0*resolDcha);  //El controlador se diseñó para interrupciones tipo RISING, hemos cambiado a CHANGE para odometría más precisa, de ahí el dividido por 2.0
     
    //Actualizar variables
    wMotorIzda_ant = wMotorIzda;
    wMotorDcha_ant = wMotorDcha;
    pulsosIzda_ant_w = pulsosIzda_w;
    pulsosDcha_ant_w = pulsosDcha_w;
    t5 = millis();
  }
  
  //CONTROL PID DE VELOCIDAD PROPIAMENTE DICHO => CONVENCIÓN: Sentido Giro Antihorario/Izquierda es +; Giro Horario/Derecha es -; 
  if((millis()-t >= Tm*1000)){                  //Si han pasado Tm segundos y está activo el control en bucle cerrado, lo ejecutamos

    //MAQUINA DE ESTADOS
    switch(FSM){
      case INICIO:
        refVelocidadDcha=0.0;
        refVelocidadIzda=0.0;
        refPhi=0.0;
        FSM=RECEPCION_DESTINO;
      break;
      case RECEPCION_DESTINO:
        if(destino) {
          FSM=COMPROBACION_PHI_X;
          destino = 0; 
        }
      break;
      case COMPROBACION_PHI_X:
        if((xDes-x)>0.03)      refPhi=0.0*(PI/180.0);
        else if((xDes-x)<-0.03) refPhi=180.0*(PI/180.0);
        else FSM=COMPROBACION_PHI_Y;                                              //Este else nunca se ejecutará porque siempre es machacado
        if(abs(Phi-refPhi)<=1.0*(PI/180.0)) FSM = DESPLAZAMIENTO_X;
        else FSM = CORRECCION_PHI_X;                                              //Si no, antes de desplazarnos corregimos la orientación
      break;
      case CORRECCION_PHI_X:
        controlPhi();
        if(t8==0 || abs(ekPhi)>1.0*(PI/180.0)) t8=millis();
        if(millis()-t8>=500){
          t8=0;                                                                  //Se reutilizará para la temporización en régimen permanente en un próximo estado
          FSM = DESPLAZAMIENTO_X;
          recto(avanza, detener); 
          resetErrorPhi = 1; 
          resetErrorMot = 1; 
        } 
      break;
      case DESPLAZAMIENTO_X:
        refVelocidadIzda=50.0;
        refVelocidadDcha=50.0;
        if(abs(xDes-x)<0.03){
          recto(avanza,detener);
          FSM = COMPROBACION_PHI_Y;
          resetErrorMot = 1; 
        } 
      break;
      case COMPROBACION_PHI_Y:
        if((yDes-y)>0.03)      refPhi=90.0*(PI/180.0);
        else if((yDes-y)<-0.03) refPhi=270.0*(PI/180.0);
        else FSM=INICIO;                                                         //Este else nunca se ejecutará porque siempre es machacado
        if(abs(Phi-refPhi)<=1.0*(PI/180.0)) FSM = DESPLAZAMIENTO_Y;
        else FSM = CORRECCION_PHI_Y;
      break;
      case CORRECCION_PHI_Y:
        controlPhi();
        if(t8==0 || abs(ekPhi)>1.0*(PI/180.0)) t8=millis();
        if(millis()-t8>=500){
          t8=0;                                                                  //Se reutilizará para la temporización en régimen permanente en un próximo estado
          FSM = DESPLAZAMIENTO_Y;
          recto(avanza, detener); 
          resetErrorPhi =  1; 
          resetErrorMot = 1; 
        }   
      break;
      case DESPLAZAMIENTO_Y:
        refVelocidadIzda=50.0;
        refVelocidadDcha=50.0;
        if(abs(yDes-y)<0.03){
          recto(avanza,detener);
          if (abs(xDes-x)<0.05) FSM = INICIO;
          else FSM = COMPROBACION_PHI_X; 
          resetErrorMot = 1; 
        } 
      break;
    }

    //Actualizar temporización de controladores 
    modo6();
    t4 = t;
    t = millis();
  }
  
  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 't' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo){                                    
    //Motor izquierdo
    if (UrealIzq > 0) MotorIzquierdo(izquierda,UrealIzq);
    else if (UrealIzq < 0) MotorIzquierdo(derecha,-UrealIzq); 
    else MotorIzquierdo(avanza,detener); 
    //Motor derecho
    if (UrealDer > 0) MotorDerecho(izquierda,UrealDer);
    else if (UrealDer < 0) MotorDerecho(derecha,-UrealDer); 
  }
  else{
    MotorIzquierdo(avanza,detener);
    MotorDerecho(avanza,detener); 
  }
/*
  //AJUSTE EN LÍNEA DE LAS CONSTANTES DEL CONTROL
  if (millis()- t2 >= 100){                              //Cada 100 ms, leer el teclado
  if (recepcion == 'l') ladoAjuste = !ladoAjuste;        //Con cada pulsación de l, se alternan los controles que podemos cambiar en línea (izda - dcha)
  else if (recepcion == 'z') estadoEncoder = 0;          //Esta tecla resetea el estado sin usar el botón de reset del arduino
  else if (recepcion =='1') ajuste*=10;
  else if (recepcion =='2') ajuste*=0.1; 
  else if (recepcion =='x'){                             //Resetear integrales del error
   Int_err_Izq = 0;
   Int_err_Der = 0;
  }
  else if (recepcion =='t'){
   telemetria = !telemetria;                             //Comenzar el experimento enviando los datos de la telemetría
   modo = !modo;                                         //Se alterna el modo (que el control actúe o no)
  }
  //else if(recepcion == 'y')refVelocidad+=ajuste;
  //else if(recepcion == 'h')refVelocidad-=ajuste;
  else if (ladoAjuste == izquierda){
  if (recepcion== 'q') KpIzq+=ajuste;
  else if (recepcion=='a') KpIzq-=ajuste;
  else if (recepcion =='w') TiIzq+=ajuste;
  else if (recepcion =='s') TiIzq-=ajuste;
  else if (recepcion =='e') TdIzq+=ajuste;
  else if (recepcion =='d') TdIzq-=ajuste;
  else if (recepcion =='r') UeqIzq+=ajuste;
  else if (recepcion =='f') UeqIzq-=ajuste;}
  else if (ladoAjuste == derecha){
  if (recepcion== 'q')   KpDer+=ajuste;
  else if (recepcion=='a') KpDer-=ajuste;
  else if (recepcion =='w') TiDer+=ajuste;
  else if (recepcion =='s') TiDer-=ajuste;
  else if (recepcion =='e') TdDer+=ajuste;
  else if (recepcion =='d') TdDer-=ajuste;
  else if (recepcion =='r') UeqDer+=ajuste;
  else if (recepcion =='f') UeqDer-=ajuste;}
   
  recepcion = 185;                                       //Resetear la tecla en memoria cuando no se esté pulsando
  t2 = millis();
  }*/
  //"TELEMETRÍA"
  
  //Leer de Putty

  if(Serial1.available()>0){ 
      bufferRecepcion[i]=Serial1.read();
      if(bufferRecepcion[i]=='\r'){
        bufferRecepcion[i]='\0';
        Serial1.println(bufferRecepcion); 
        sscanf(bufferRecepcion,"x:%d y:%d",&xDesInt,&yDesInt);
        xDes = ((float) xDesInt)/100.0;
        yDes = ((float) yDesInt)/100.0; 
        destino = 1; 
        modo=1; 
        i=0;
      }
      else if (bufferRecepcion[i]=='t'){                          //Parada de emergencia 
        modo=!modo; 
        FSM = INICIO; 
        resetErrorMot = 1; 
        resetErrorPhi = 1; 
      }
      else i++;  
    }
  /*
  //Escribir en Putty
  Serial1.print("   x = ");
  Serial1.print(x);
  Serial1.print("   y = ");
  Serial1.print(y);
  Serial1.print("   xDes = ");
  Serial1.print(xDes);
  Serial1.print("   yDes = ");
  Serial1.print(yDes);
  Serial1.print("  refPhi = "); 
  Serial1.print(refPhi*180/PI);
  Serial1.print("   phi(º) = ");
  Serial1.print(Phi*180.0/PI);
  Serial1.print(" Est= ");
  Serial1.print(fsm[FSM]);
  Serial1.print(" Dest= ");
  Serial1.print((int) destino);
  Serial1.print(" eKPhi = "); 
  Serial1.print(ekPhi);
  Serial1.print(" uKDer = ");
  Serial1.print(ukDer);
  Serial1.print(" uKIzq = ");
  Serial1.print(ukIzq);  
  Serial1.print(" t8 = "); 
  Serial1.print(t8); 
  Serial1.print(" tRegPer = "); 
  Serial1.print(millis()-t8);   
  Serial1.print(" resPhi= "); 
  Serial1.print(resetErrorPhi);  
  Serial1.print(" resMot= "); 
  Serial1.print(resetErrorMot); 
  Serial1.println();*/
  
  //TELEMETRÍA
  /*
  //Leer de Putty
  if(Serial1.available()>0){
    recepcion = Serial1.read();      
  }*/
  
  //Escribir en Putty
  if(FSM != RECEPCION_DESTINO && FSM != INICIO){   
  Serial1.print(millis());                     //dat1: tiempo del micro
  Serial1.print(",");
  
  //Sensores
  Serial1.print(wMotorIzda);                   //dat2: velocidad angular del motor izquierdo
  Serial1.print(",");
  Serial1.print(wMotorDcha);                   //dat3: velocidad angular del motor derecho
  Serial1.print(",");
  Serial1.print(refActualIzda);                //dat4: referencia FILTRADA de velocidad angular para motor izquierdo
  Serial1.print(",");
  Serial1.print(refActualDcha);                //dat5: referencia FILTRADA de velocidad angular para motor derecha
  Serial1.print(",");
  Serial1.print(x);                            //dat6: distancia recorrida en el eje x
  Serial1.print(",");
  Serial1.print(y);                            //dat7: distancia recorrida en el eje y
  Serial1.print(",");
  Serial1.print(Phi);                          //dat8: orientación del robot
  Serial1.print(",");
  Serial1.print(incX);                         //dat9: incremento de distancia en el eje x
  Serial1.print(",");
  Serial1.print(incY);                         //dat10: incremento de distancia en el eje y
  Serial1.print(",");
  Serial1.print(incPhi);                       //dat11:  incremento de orientación 
  Serial1.print(",");
  Serial1.print(pulsosIzda);                   //dat12: pulsos en el encoder izquierdo desde el arranque del programa
  Serial1.print(",");
  Serial1.print(pulsosDcha);                   //dat13: pulsos en el encoder derecho desde el arranque del programa
  Serial1.print(",");
  
  //Actuadores
  Serial1.print(UrealIzq);                     //dat14: escritura del actuador izquierdo (pwm)
  Serial1.print(",");
  Serial1.print(UrealDer);                     //dat15: escritura del actuador derecho (pwm)
  Serial1.print(",");
  Serial1.print(FSM);                          //dat16: estado de la FSM 
  Serial1.print(";");
  Serial1.println();
  }
  
}                                             //Fin void loop() 
  

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    sentGiroIzq = avanza; 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    sentGiroIzq = retrocede;
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    sentGiroDer = avanza; 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    sentGiroDer = retrocede; 
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.902*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}

void contadorPulsosIzda(){              //Rutina de interrupción para contar número de pulsos de los encoders
    if (sentGiroIzq == avanza)
    {
      pulsosIzda++;
    }
    else
    {
      pulsosIzda--;
    }
}
void contadorPulsosDcha(){              //Rutina de interrupción para contar número de pulsos de los encoders

    if (sentGiroDer == avanza)
    {
      pulsosDcha++;
    }
    else
    {
      pulsosDcha--;
    }
}

void ActualizarOdometria(){                           //Funcion que actualiza la odometría del robot
  float Lizda=0.0;
  float Ldcha=0.0;
  float Lcentro=0.0;
  
  pulsosIzda_odom = pulsosIzda;
  pulsosDcha_odom = pulsosDcha;
  
  //CALCULAR INCREMENTOS
  //ODOMETRÍA POR VELOCIDADES
  /*
  v = (c/2.0)*(wMotorIzda_ant + wMotorDcha_ant)*(2.0*PI/60.0);      //Velocidad en el instante anterior para calcular la odometría actual, y pasando las velocidades angulares a rad/s
  incPhi = (c/b)*(wMotorDcha_ant-wMotorIzda_ant)*(2.0*PI/60.0)*inc_k; 
  incX = v*(cos(Phi_ant))*inc_k;
  incY = v*(sin(Phi_ant))*inc_k; 
  */

  //ODOMETRÍA POR DISTANCIA RECORRIDA
  // /*
  Lizda = (pulsosIzda_odom-pulsosIzda_ant_odom)*2.0*c*PI/resolIzda;
  Ldcha = (pulsosDcha_odom-pulsosDcha_ant_odom)*2.0*c*PI/resolDcha;
  Lcentro = (Lizda+Ldcha)/2.0;

  //LizdaNoReset = pulsosIzdaNoReset*2.0*c*PI/resolIzdaPhi;
  //LdchaNoReset = pulsosDchaNoReset*2.0*c*PI/resolDchaPhi;
  
  incPhi = (resolIzda/resolIzdaPhi)*(Ldcha-Lizda)/b; 
  incX = Lcentro*cos(Phi); 
  incY = Lcentro*sin(Phi); 
  // */

  //CALCULAR ODOMETRÍA CON LOS INCREMENTOS OBTENIDOS
  x = x_ant + incX;
  y = y_ant + incY;
  Phi = Phi_ant + incPhi;
  //Phi = (LdchaNoReset-LizdaNoReset)/b;
  //Phi = atan2(sin(Phi),cos(Phi));    //Mantener Phi entre [-180,180]
  //while(Phi>=360.0*(PI/180.0))Phi = Phi-360.0*(PI/180.0);
  //while(Phi<0.0*(PI/180.0))  Phi = Phi+360.0*(PI/180.0);
  
  //ACTUALIZACIÓN  
  x_ant = x;
  y_ant = y;
  Phi_ant = Phi; 
  
  pulsosIzda_ant_odom = pulsosIzda_odom;
  pulsosDcha_ant_odom = pulsosDcha_odom;  
  inc_k = (millis()-t7)/1000.0;
  t7 = millis();
}

void modo6(){   
    //Reinicio error integral 
    if (resetErrorMot)
    {
      Int_err_Der = 0.0;
      Int_err_Izq = 0.0; 
      resetErrorMot = 0; 
    }
    //Filtrado de la referencia
    if (modo){
      refActualIzda = alpha*refVelocidadIzda + (1-alpha)*refAntIzda;
      refActualDcha = alpha*refVelocidadDcha + (1-alpha)*refAntDcha;
    }
    else{ 
      refActualIzda = 0.0;
      refAntIzda = 0.0;
      refActualDcha = 0.0;
      refAntDcha = 0.0;
    }

    //Cálculo del error para los dos controladores:
    ekIzq = refActualIzda - wMotorIzda;
    ekDer = refActualDcha - wMotorDcha;

    //Implementación de controladores PID
    ukIzq = KpIzq * ekIzq + (Int_err_Izq / TiIzq) + TdIzq * ((ekIzq - ek1Izq) / Tm);  //PID Izquierdo
    ukDer = KpDer * ekDer + (Int_err_Der / TiDer) + TdDer * ((ekDer - ek1Der) / Tm);  //PID Derecho  

    //Zona muerta y saturación del control izquierdo: LA ZONA MUERTA ES LA MISMA QUE LA DE LOS MODOS 1 AL 4: si no hay un mínimo de Ueq,
    //el motor no puede tener velocidad no nula
    if(ukIzq>=0)Uk_ssIzq = ukIzq; //Salvar zona muerta por la zona positiva del control izquierdo sumando señal de equilibrio
    if(ukIzq<0)Uk_ssIzq =  ukIzq; //Salvar zona muerta por la zona negativa del control izquierdo restando señal de equilibrio
    //Saturación del controlador IZQUIERDO [Umin = -180; Umax = 180]
    if (Uk_ssIzq < Umin)UrealIzq = Umin;
    else if (Uk_ssIzq > Umax)UrealIzq = Umax;
    else UrealIzq = Uk_ssIzq; 
    if(Uk_ssIzq<Umax && Uk_ssIzq>Umin)Int_err_Izq += ekIzq * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Zona muerta y saturación del control derecho
    if(ukDer>=0)Uk_ssDer = ukDer; //No salvamos la zona muerta en este experimento
    if(ukDer<0)Uk_ssDer = ukDer;  //No salvamos zona muerta en este experimento
    //Saturación del controlador DERECHO [Umin = -180; Umax = 180]
    if (Uk_ssDer < Umin)UrealDer = Umin;
    else if (Uk_ssDer > Umax)UrealDer = Umax;
    else UrealDer = Uk_ssDer; 
    if(Uk_ssDer<Umax && Uk_ssDer>Umin)Int_err_Der += ekDer * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Actualizar variables
    ek1Izq = ekIzq;
    ek1Der = ekDer;
    refAntIzda = refActualIzda;
    refAntDcha = refActualDcha;
}

void controlPhi(){   
   
   //Filtrado de la referencia
    if (modo){
      refActualPhi = alphaPhi*refPhi + (1-alphaPhi)*refAntPhi;
    }
    else{ 
      refActualPhi = 0.0;
      refAntPhi = 0.0;
    }

    if (resetErrorPhi)
    {
      Int_err_Phi = 0; 
      resetErrorPhi = 0; 
    }
    //Hacer que refPhi € [0,360]º
    // while(refPhi>=360.0*(PI/180.0))refPhi = refPhi-360.0*(PI/180.0);
    //while(refPhi<0.0*(PI/180.0))   refPhi = refPhi+360.0*(PI/180.0);

    //Cálculo del error para el controlador
    ekPhi = refActualPhi - Phi;

    //Implementación de controladores PID
    ukPhi = KpPhi * ekPhi + (Int_err_Phi / TiPhi) + TdPhi * ((ekPhi - ek1Phi) / Tm);  //PID Phi

    Uk_ssPhi = ukPhi;
    //Saturación del controlador [UminPhi = -180; UmaxPhi = 180]
    if (Uk_ssPhi < UminPhi)UrealPhi = UminPhi;
    else if (Uk_ssPhi > UmaxPhi)UrealPhi = UmaxPhi;
    else UrealPhi = Uk_ssPhi; 
    if(Uk_ssPhi<UmaxPhi && Uk_ssPhi>UminPhi)Int_err_Phi += ekPhi * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Actualizar variables
    ek1Phi = ekPhi;
    refAntPhi = refActualPhi; 

    //Conectar ambos controladores
    refVelocidadDcha = UrealPhi;
    refVelocidadIzda = -UrealPhi;

  
}
