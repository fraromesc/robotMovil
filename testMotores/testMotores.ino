//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDer 52
#define echoDer 50

#define trigIzq 24
#define echoIzq 22

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que  avanzando hacia izda o dcha a cierta velocidad

//PARÁMETROS DEL CONTROL 
float Ureal=0;     //Señal de control que se aplica
float Umin=-255;   //Señal de control mínima
float Umax=255;    //Señal de control máxima
float Uk_ss=0;     //Señal de control intermedia (antes del anti-windup)
float Ueq=0;       //Valor de equilibrio de la señal de control
float ek=0;        //Error en k
float ek1=0;       //Error en k-1
float ekDer = 0;   //Error en k del motor derecho 
float ekIzq = 0;   //Error en k del motor izquierdo
float Kp;          //Cte proporcional para el control 
float Ti;          //Tiempo integral para el control
float Td;          //Tiempo derivativo para el control  
float uk=0;        //Señal de control instante k 
float Tm=0.01;     //Periodo de muestreo EN SEGUNDOS
float Int_err=0;   //Integral del error
float ref;         //Referencia para el control
unsigned long t=0; //Variable de tiempo para esperar Tm
int estado=0;      //Estado de la máquina de estados para elegir la referencia del control 

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  Serial.begin(9600);

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);
}


  
void loop() {

  char recepcion;
  //Recepción del valor
  if(Serial.available()>0){
        recepcion = Serial.read();      
  }
  
  Serial.println(recepcion);
  
  switch(recepcion){
    case 'q':
      MotorIzquierdo(avanza,80);
      break;
    case 'w':
      MotorDerecho(avanza,80);
      break;
    case 'a':
      MotorIzquierdo(retrocede,80);
      break;
    case 's':
      MotorDerecho(retrocede,80);
      break;
    case 'e':
      recto(avanza,80);
      break;
    case 'd':
      recto(retrocede,80);
      break;
    case 'r':
      giro(izquierda,avanza,80);
      break;
    case 'f':
      giro(derecha,avanza,80);
      break;
    case 't':
      giro(izquierda,retrocede,80);
      break;
    case 'g':
      giro(derecha,retrocede,80);
      break;
    case 'y':
      pivota(izquierda,80);
      break;
    case 'h':
      pivota(derecha,80);
      break;
    case 'u':
      recto(avanza,detener);
      break;
  }
}

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   long duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, (85/80)*pwm);             //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}
