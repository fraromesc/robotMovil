//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDer 52
#define echoDer 50

#define trigIzq 25
#define echoIzq 22

//CABECERAS DE FUNCIONES
float distanciaUS(int TriggerPin, int EchoPin);       //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que  avanzando hacia izda o dcha a cierta velocidad

//PARÁMETROS DEL CONTROL 
float UeqIzq=65;       //Valor de equilibrio de la señal de control, necesario para salvar la zona muerta
float UeqDer=60;       //Valor de equilibrio de la señal de control, necesario para salvar la zona muerta
float Umin=-180;   //Señal de control mínima
float Umax=180;    //Señal de control máxima
//CONTROL IZQUIERDO
float UrealIzq=0;  //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0;  //Señal de control intermedia (antes del anti-windup)
float KpIzq=10.0;  //Cte proporcional para el control del motor izquierdo
float TiIzq=100;   //Tiempo integral para el control del motor izquierdo
float TdIzq=0.01;     //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0;//Integral del error izquierdo  
float ukIzq=0;     //Señal de control instante k para el controlador izquierdo
float ekIzq=12;     //Error en k para el controlador izquierdo
float ek1Izq=0;    //Error en k-1 para el controlador izquierdo
float distIzq=0;     //Distancia medida por el ultrasonidos izquierdo
float FiltdistIzq=0;  //Distancia medida por el ultrasonidos izquierdo y filtrada en el instante actual
float FiltdistIzqAnt=0;  //Distancia medida por el ultrasonidos izquierdo y filtrada en el instante anterior
//CONTROL DERECHO
float UrealDer=0;  //Señal de control que se aplica al motor derecho
float Uk_ssDer=0;  //Señal de control intermedia (antes del anti-windup)
float KpDer=10.0*0.96;  //Cte proporcional para el control del motor derecho
float TiDer=100*0.96;   //Tiempo integral para el control del motor derecho
float TdDer=0.01*0.96;     //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0;//Integral del error derecho  
float ukDer=0;     //Señal de control instante k para el controlador derecho
float ekDer=12;     //Error en k para el controlador derecho
float ek1Der=0;    //Error en k-1 para el controlador derecho
float distDer=0;     //Distancia medida por el ultrasonidos derecho
float FiltdistDer=0;  //Distancia medida por el ultrasonidos derecho y filtrada en el instante actual
float FiltdistDerAnt=0;  //Distancia medida por el ultrasonidos derecho y filtrada en el instante anterior


float Tm=0.01;     //Periodo de muestreo EN SEGUNDOS
float Int_err=0;   //Integral del error
float ref=30;      //Referencia para el control
unsigned long t=0; //Variable de tiempo para esperar Tm
unsigned long t2=0;//Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;//Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
float ajuste = 1;  //Variable para realizar ajuste fino/grueso de las constantes del control
int estado = 0;    //Estado de la máquina de estados para elegir la referencia del control
bool modo = false; //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false; //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
unsigned char recepcion = 185;

  //BUFFER CIRCULAR PARA FILTRO
  float buff[100];
  int i = 0;
  bool lleno = 0;
  float media = 0.0;
  float sumaParcial = 0.0;
  float desvTip = 0.0;



void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigIzq,OUTPUT);
  pinMode(echoIzq,INPUT);
  pinMode(trigDer,OUTPUT);
  pinMode(echoDer,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    // Comunicación por puerto serie arduino
  Serial1.begin(38400);  // Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);
}

 
void loop() {

  //CONTROLADORES
  if (millis()- t >= Tm*1000)                         //Esperar Tm (millis-t == tiempo desde que se ejecutó el control la última vez EN MILISEGUNDOS)
  {                                                   //Tm * 1000 = pasar Tm A MILISEGUNDOS

    //Medida de los ultrasonidos
    distIzq=distanciaUS(trigIzq, echoIzq);
    distDer=distanciaUS(trigDer, echoDer);

    //Llenar buffer
    if(!lleno){  //Si no está lleno, llenar el buffer
    buff[i]=distDer;
    i++;
    }
    
    if(i>=100)lleno=1;
      
    if(lleno){ //Si el buffer se llenó, realizar un desplazamiento circular y por último meter en la posición 0 la nueva medida
      //Recalcular el buffer circularmente
      for(i=100;i>-1;i--){
        buff[i]=buff[i-1];
      }
      buff[0]=distDer;

      //Calcular la media 
      sumaParcial=0.0;
      for(i=0;i<100;i++)sumaParcial+=buff[i];
      media = sumaParcial/100;

      //Calcular la desviación típica
      sumaParcial = 0.0;
      for(i=0;i<100;i++)sumaParcial+=(pow((buff[i]-media),2));
      desvTip=sqrt(sumaParcial/100);     
    }

    //Filtrado de las medidas del ultrasonidos derecho
    FiltdistDer=distDer;
    if((distDer-media)>desvTip)FiltdistDer=media;
  }
 
  //"TELEMETRÍA"
   
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  Serial.print(distDer);
  Serial.print(",");
  Serial.println(media);
}

//DEFINICIÓN DE LAS FUNCIONES
float distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.902*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}
