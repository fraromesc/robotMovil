//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ULTRASONIDOS
#define trigDer 52
#define echoDer 50

#define trigIzq 25
#define echoIzq 22

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);         //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);          //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);            //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                     //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                   //Función para que  avanzando hacia izda o dcha a cierta velocidad

//PARÁMETROS DEL CONTROL 
float Ureal=0.0;               //Señal de control que se aplica
float Umin=-180.0;             //Señal de control mínima
float Umax=180.0;              //Señal de control máxima
float Uk_ss=0.0;               //Señal de control intermedia (antes del anti-windup)
float Ueq=65.0;                //Valor de equilibrio de la señal de control
float ek=0.0;                  //Error en k
float ek1=0.0;                 //Error en k-1
int distIzq=0;                 //Distancia medida por el ultrasonidos izquierdo
int distDer=0;                 //Distancia medida por el ultrasonidos derecho
float ekDer=0.0;               //Error en k del motor derecho 
float ekIzq=0.0;               //Error en k del motor izquierdo
float Kp=10.0;                 //Cte proporcional para el control 
float Ti=100.0;                //Tiempo integral para el control
float Td=0.2;                  //Tiempo derivativo para el control  
float uk=0;                    //Señal de control instante k 
float Tm=0.01;                 //Periodo de muestreo EN SEGUNDOS             
float Int_err=0.0;             //Integral del error
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre lecturas del bluetooth para enviar por telemetría como primer dato
float ajuste = 1;              //Variable para realizar ajuste fino/grueso de las constantes del control
int estado = 0;                //Estado de la máquina de estados para elegir la referencia del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado
float distIzqAnt = 0.0;        //Distancia medida por el ultrasonidos izquierdo en el instante anterior
float distDerAnt = 0.0;        //Distancia medida por el ultrasonidos derecho en el instante anterior

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  
  //PINES DE LOS ULTRASONIDOS
  pinMode(trigIzq,OUTPUT);
  pinMode(echoIzq,INPUT);
  pinMode(trigDer,OUTPUT);
  pinMode(echoDer,INPUT);

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    // Comunicación por puerto serie arduino
  Serial1.begin(38400);  // Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);
}

 
void loop() {

  //CONTROLADOR
  if (millis()- t >= Tm*1000)                                //Esperar Tm (millis-t == tiempo desde que se ejecutó el control la última vez EN MILISEGUNDOS)
  {                                                          //Tm * 1000 = pasar Tm A MILISEGUNDOS

    //Medida de los ultrasonidos
    distIzq=distanciaUS(trigIzq, echoIzq);
    distDer=distanciaUS(trigDer, echoDer);

    //Filtro de medidas
    if(millis()>1000){  //No filtramos las primeras medidas, filtramos cuando ya se han actualizado varias veces las medidas del instante anterior
      if((distIzq-distIzqAnt)>30)distIzq=0.1*distIzq+distIzqAnt;
      if((distDer-distDerAnt)>30)distDer=0.1*distDer+distDerAnt;
    }
    //Cálculo de errores
    ekDer=distDer-ref;          
    ekIzq=distIzq-ref; 
    ek=(ekDer+ekIzq)/2;                                      //Cálculo del error para el controlador como la media de los dos errores (MODO 1)

    //Implementación de controlador PID
    uk = Kp * ek + (Int_err / Ti) + Td * ((ek - ek1) / Tm);  //PID Completo
  
    //Zona muerta y saturación del control derecho
    if(uk>=0)Uk_ss = Ueq+uk; //Salvar zona muerta por la zona positiva del control sumando señal de equilibrio
    if(uk<0)Uk_ss = -Ueq+uk; //Salvar zona muerta por la zona negativa del control restando señal de equilibrio
    //Saturación del controlador [Umin = -180; Umax = 180]
    if (Uk_ss < Umin)Ureal = Umin;
    else if (Uk_ss > Umax)Ureal = Umax;
    else Ureal = Uk_ss; 
    if(Uk_ss<Umax && Uk_ss>Umin)Int_err += ek * Tm; //Anti-windup: sólo para cuando tenemos efecto integral
    }
    
    //Actualizar variables
    ek1 = ek;
    t = millis();

    distIzqAnt = distIzq;
    distDerAnt = distDer;

    //Esperar en régimen permanente
    if(!((ek <= 1 && ek >= -1))) t3 = millis();         //Si el error NO es cero (error permitido 1 cm), 
                                                        //actualizar t3 a millis, si sí lo es, comenzar la espera de régimen permanente
 
    //Máquina de estados para decidir la referencia     //Idea: repetir TEST transparencias para el MODO 1
    if(((millis()-t3)>=3000)&& estado < 2 )estado++;    //Si se alcanza la referencia durante 3 segundos al menos, cambiar de referencia
    switch (estado){
      case 0:                                           //Estado == 0 => Referencia = 30 cm
        ref=30;
        break; 
      case 1:                                           //Estado == 1 => Refererncia = 50 cm 
        ref=50; 
        break;  
      default:                                          //Estado == 2 => Detener el coche pues se ha alcanzado la referencia final (da igual la referencia)
        break; 
    }

  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 'm' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo && estado < 2){                               //Estado == 2 => Detener el coche pues se ha alcanzado la referencia final (50 cm)
    
    if(distIzq-distDer<15){                             //Si la diferencia de los ultrasonidos no es muy acusada, continuar con el control 
    if (Ureal > 0) recto(avanza,Ureal);
    else if (Ureal < 0) recto(retrocede,-Ureal); 
    else recto(avanza,detener); 
    }
    else{                                               //Si la diferencia entre medidas sí es fuerte, girar para apuntar el robot de nuevo hacia la pared
      if(distIzq>distDer)pivota(derecha,80);
      else pivota(izquierda,80);
    }
    
  }
  else recto(avanza,detener);                           //Si se ha terminado el experimento o se ha desactivado el control, detener el robot


  //AJUSTE EN LÍNEA DE LAS CONSTANTES DEL CONTROL
  if (millis()- t2 >= 100){                           //Cada 100 ms, leer el teclado
   if (recepcion== 'q') Kp+=ajuste;
   else if (recepcion=='a') Kp-=ajuste; 
   else if (recepcion =='w') Ti+=ajuste;
   else if (recepcion =='s') Ti-=ajuste; 
   else if (recepcion =='e') Td+=ajuste;
   else if (recepcion =='d') Td-=ajuste;
   else if (recepcion =='1') ajuste*=10;
   else if (recepcion =='2') ajuste*=0.1;
   else if (recepcion =='t'){
    telemetria = !telemetria; //Comenzar el experimento enviando los datos de la telemetría
    modo = !modo;             //se alterna el modo (que el control actúe o no)
   }
   else if (recepcion == 'z') estado = 0;              //Esta tecla resetea el estado sin usar el botón de reset del arduino

   recepcion = 185;                                    //Resetear la tecla en memoria cuando no se esté pulsando
   t2 = millis();
  }

 
  //"TELEMETRÍA"
  /*
  //Leer de Putty
  if(Serial1.available()>0){
    recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  Serial1.print("USIzdo = ");
  Serial1.print(distIzq);
  Serial1.print("   USDcho = ");
  Serial1.print(distDer);
  Serial1.print("   Ureal = ");
  Serial1.print(Ureal);
  Serial1.print("   Ref = ");
  Serial1.print(ref);
  Serial1.print("   Kp = ");
  Serial1.print(Kp);
  Serial1.print("   Ti = ");
  Serial1.print(Ti);
  Serial1.print("   Td = ");
  Serial1.print(Td);
  Serial1.print("   ajuste = ");
  Serial1.print(ajuste);
  Serial1.print("   estado = ");
  Serial1.print(estado);
  Serial1.print("   recepcion=   "); 
  Serial1.print(recepcion);
  Serial1.print("   t2=   "); 
  Serial1.print(t2);
  Serial1.println();
  //Serial1.print("   recepcion=   "); 
  //Serial1.print(recepcion);
  //Serial1.print("   t2=   "); 
  //Serial1.print(t2);
  //Serial1.println();
  */
  //Serial.print("   recepcion=   "); 
  //Serial.print(recepcion);
  //Serial.print("   t2=   "); 
  //Serial.print(t2);
  //Serial.println();
  //TELEMETRÍA
  
  //Leer de Putty
  if(Serial1.available()>0){
    recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  if(telemetria){
  Serial1.print(millis()-t4);                    //dat1: tiempo entre lecturas del bluetooth
  Serial1.print(",");
  //Sensores
  Serial1.print(distIzq);                        //dat2: lectura del sensor izquierdo
  Serial1.print(",");
  Serial1.print(distDer);                        //dat3: lectura del sensor derecho
  Serial1.print(",");
  Serial1.print(ref);                            //dat4: referencia
  Serial1.print(",");
  
  //Actuadores
  Serial1.print(Ureal);                          //dat5: escritura del actuador izquierdo
  Serial1.print(",");
  Serial1.print(0.960*Ureal);                    //dat6: escritura del actuador derecho: el factor de corrección entre ruedas es de 0.960 (ver función recto)
  Serial1.print(",");
  Serial1.print(millis());                       //dat7: milisegundos desde que arrancó el programa, para comparar con las marcas temporales de Tm
  Serial1.print(";");
  Serial1.println(); 
  }

  t4 = millis();                                 //Actualizar t4 para el cálculo del tiempo entre lecturas del bluetooth
  
}

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   long duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.960*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}
