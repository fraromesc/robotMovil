#include <math.h>
#define velPivote 30.0
//Consideramos que el arduino está en la parte trasera derecha, por tanto izda y dcha se definen así:
//MOTOR DERECHO    
#define IN1 4
#define IN2 5
#define ENA 3

//MOTOR IZQUIERDO
#define IN3 7
#define IN4 8
#define ENB 6

//DEFINE PARA LAS FUNCIONES DE CONTROL DE MOTORES Y COCHE
#define avanza 0
#define retrocede 1
#define detener 0
#define izquierda 0
#define derecha 1

//PINES PARA ENCODERS
#define pinEncoderIzda 20                             //asociada a INT3 => attachInterrupt(3, ..., ...)
#define pinEncoderDcha 21                             //asociada a INT2 => attachInterrupt(2, ..., ...)

//PINES IR
#define IRIzqExt 22
#define IRIzq 25
#define IRCen 37
#define IRDer 50
#define IRDerExt 52

//LECTURA NEGADA DE LOS IR para forzar lógica alta al detectar la cinta
#define lecturaIRIzqExt     digitalRead(IRIzqExt)
#define lecturaIRIzq        digitalRead(IRIzq)
#define lecturaIRCen        digitalRead(IRCen)
#define lecturaIRDer        digitalRead(IRDer)
#define lecturaIRDerExt     digitalRead(IRDerExt)
//VALORES GEOMETRICOS DEL COCHE
#define Lx  14.5
#define Ly 7.5
//Estados FMS 
#define INICIO                  0
#define ESPERADESTINO           1
#define PLANIFICADORTRAYECTORIA 2
#define XDESPLAZAMIENTO         3
#define YDESPLAZAMIENTO         4
#define OBJETIVO                5
#define ERROR                   6

char *fms []={  "INICIO  ",
                "ESPDESTO",
                "PLANTRAY", 
                "XDESPLTO", 
                "YDESPLTO", 
                "OBJETIVO",
                "ERROR   "}; 

//Estados FMS desplazamiento
#define AVANZA                  0
#define COMPROBACIONODOM        1
#define ODOMCALIBRADA           2
#define CALIBRACION             3
#define GIRAORI                 4

char *fmsDesp[]={"AVANZA  ",
                 "COMPODOM",
                 "ODOMCALI",
                 "CALIBRAC",
                 "GIRAORI "};
//Estados FMS giro
#define COMPROBACIONORI         0
#define APROXIMACIONX           1
#define APROXIMACIONY           2
#define FRENADO                 3
#define PIVOTEAR                4

char *fmsGiro[]={
  "COMPORI",
  "APROX X",
  "APROX Y",
  "FRENADO",
  "PIVOTEA",
};
#define CARACTERVACIO           185

int sign(float x) 
{
  return ((x>0) - (x < 0));}

//CABECERAS DE FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin);               //Función para medir la distancia por ultrasonidos
void MotorIzquierdo(bool sentGiro, int pwm);                //Función para controlar directamente el motor izquierdo
void MotorDerecho(bool sentGiro, int pwm);                  //Función para controlar directamente el motor derecho
void recto(bool sentido,int pwm);                           //Función para hacer que el coche avance o retroceda a cierta velocidad
void giro(bool sentGiro, bool sentAvance,int pwm);          //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
void giroBrusco(bool sentGiro, bool sentAvance,int pwm);    //Función para hacer que gire bruscamente hacia izda o dcha a cierta velocidad
void pivota(bool sentido, int pwm);                         //Función para que avanzando hacia izda o dcha a cierta velocidad
void contadorPulsosIzda();                                  //Rutina de interrupción para contar número de pulsos de los encoders
void contadorPulsosDcha();                                  //Rutina de interrupción para contar número de pulsos de los encoders
void ActualizarOdometria();                                 //Funcion que actualiza la odometría del robot
void modo6();                                               //Función para regular la velocidad angular de las ruedas 

//PARÁMETROS DE LAS FMS 
char FMS = INICIO;              //Variable para el estado de la FMS principal del sistema
char FMSDesp = GIRAORI;          //Variable para el estado de la FMS secundaria, dedicada al desplazamiento en X e Y
char FMSGiro = COMPROBACIONORI;
char orden = CARACTERVACIO;               //Orden recibida desde teclado para el sistema

//PARAMETROS DEL MAPA
char fil[6]={0,1,2,0,1,2};
char col[6]={0,1,0,1,0,1}; 
char nodoDestX, nodoDestY, nodoAntX=0, nodoAntY=0, ori=1, oriDes=1, contX=0, contY=0;
float mapaOdomX[2][3] = {{0, 10, 20}, 
                         {0, 10, 20}}; 
float mapaOdomY[2][3]=  {{0,  0, 0},
                          {10,10,10}};
float XDes = 0.0; 
float YDes = 0.0;
float Xprima = 0.0; 
float Yprima = 0.0; 
//PARÁMETROS SENSORES INFRARROJOS
int sensoresIR = 0; 
bool medidaIRIzqExt=0, medidaIRIzq=0, medidaIRCen=0, medidaIRDer=0, medidaIRDerExt=0;
//PARÁMETROS DEL COCHE
float x = 0.0;                  //Posición en x del vehículo (inicialmente 0)   [m]
float y = 0.0;                  //Posición en y del vehículo (inicialmente 0)   [m] 
float Phi = 0.0;                //Posición en Phi del vehículo (inicialmente 0) [rad]
float x_ant = 0.0;              //Posición en x del vehículo en el instante anterior (inicialmente 0)  [m]
float y_ant = 0.0;              //Posición en y del vehículo en el instante anterior (inicialmente 0)  [m]
float Phi_ant = 0.0;            //Posición en Phi del vehículo en el instante anterior (inicialmente 0)[rad]
float c = 0.065/2.0;            //Radio de la rueda del coche [m]
float b = 0.105;                //Distancia entre las ruedas del coche [m]
float v = 0.0;                  //Velocidad de avance del coche [m/s]
float inc_k = 0.0;              //Incremento de tiempo entre actuaciones
float incX=0.0;                 //Incremento de x calculado por la odometría
float incY=0.0;                 //Incremento de y calculado por la odometría
float incPhi=0.0;               //Incremento de Phi calculado por la odometría

//PARÁMETROS DEL CONTROL 
float UeqIzq=65.0;             //Valor de equilibrio de la señal de control del lado izquierdo, necesario para salvar la zona muerta
float UeqDer=60.0;             //Valor de equilibrio de la señal de control del lado derecho, necesario para salvar la zona muerta
float Umin=-255.0;             //Señal de control mínima (para izda y dcha)
float Umax=255.0;              //Señal de control máxima (para izda y dcha)

//CONTROL DE VELOCIDAD DEL MOTOR IZQUIERDO
float UrealIzq=0.0;            //Señal de control que se aplica al motor izquierdo
float Uk_ssIzq=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpIzq=(2.742);           //Cte proporcional para el control del motor izquierdo   
float TiIzq=0.3*2.90;          //Tiempo integral para el control del motor izquierdo
float TdIzq=0.05;              //Tiempo derivativo para el control del motor izquierdo
float Int_err_Izq=0.0;         //Integral del error izquierdo  
float ukIzq=0.0;               //Señal de control instante k para el controlador izquierdo
float ekIzq=0.0;               //Error en k para el controlador izquierdo
float ek1Izq=0.0;              //Error en k-1 para el controlador izquierdo
float distIzq=0.0;             //Distancia medida por el ultrasonidos izquierdo
float distIzqAnt=0.0;          //Distancia medida por el ultrasonidos izquierdo en el instante anterior

//CONTROL DE VELOCIDAD DEL MOTOR DERECHO
float UrealDer=0.0;            //Señal de control que se aplica al motor derecho
float Uk_ssDer=0.0;            //Señal de control intermedia (antes del anti-windup)
float KpDer=(3.3670)/1.2;      //Cte proporcional para el control del motor derecho
float TiDer=0.5765*1.5;        //Tiempo integral para el control del motor derecho
float TdDer=0.01;              //Tiempo derivativo para el control del motor derecho
float Int_err_Der=0.0;         //Integral del error derecho  
float ukDer=0.0;               //Señal de control instante k para el controlador derecho
float ekDer=0.0;               //Error en k para el controlador derecho
float ek1Der=0.0;              //Error en k-1 para el controlador derecho
float distDer=0.0;             //Distancia medida por el ultrasonidos derecho
float distDerAnt=0.0;          //Distancia medida por el ultrasonidos derecho en el instante anterior

//VARIABLES ADICIONALES
float refActualIzda = 0.0;     //Referencia filtrada para el motor izquierdo
float refAntIzda = 0.0;        //Referencia anterior para el filtro del motor izquierdo
float refActualDcha = 0.0;     //Referencia filtrada para el motor derecho
float refAntDcha = 0.0;        //Referencia anterior para el filtro del motor derecho
float alpha = 0.65;            //Alpha para realizar un filtro FIR sobre la referencia 

float Tm=0.1;                  //Periodo de muestreo EN SEGUNDOS
float ref=30.0;                //Referencia para el control
unsigned long t=0;             //Variable de tiempo para esperar Tm
unsigned long t2=0;            //Variable de tiempo para esperar 100 ms para lectura para puerto BT (actualización en línea ctes control)
unsigned long t3=0;            //Variable para esperar que el error se estabilice en régimen permanente antes de cambiar la referencia
unsigned long t4=0;            //Variable para calcular el tiempo entre actuaciones para enviar por telemetría como primer dato
unsigned long t5=0;            //Variable para esperar 1s para contar pulsos de encoder por segundo
unsigned long t6=0;            //Variable para la impresión de telemetría
unsigned long t7=0;            //Variable de tiempo para incrementos de tiempo en la odometría
unsigned long t8=0;            //Variable de tiempo para tener un margen de tiempo de seguridad para la función siguelineas
float ajuste = 1.0;            //Variable para realizar ajuste fino/grueso de las constantes del control
bool modo = false;             //Estado para una segunda máquina de estados que decide entre aplicar y no aplicar la acción de control a los motores 
bool ladoAjuste = false;       //Estado para una tercera máquina de estados que decide entre constantes del lado izdo y lado dcho para cambiar en línea   
bool telemetria = false;       //Booleano para escribir/no escribir datos en Putty para enviar datos por telemetría: para evitar enviar datos mientras
                               //colocamos el robot antes de la prueba
unsigned char recepcion = 185; //Tecla pulsada para el ajuste en línea mediante teclado

//VARIABLES DE LOS ENCODERS PARA EL CONTROL DE VELOCIDAD DE LOS MOTORES
volatile float pulsosIzda=0.0;      //Contador para pulsos de encoder del motor izquierdo
volatile float pulsosDcha=0.0;      //Contador para pulsos de encoder del motor derecho
float pulsosIzda_odom=0.0;          //Contador para pulsos de encoder del motor izquierdo para la odometría. Se actualiza cada 100 ms
float pulsosIzda_w=0.0;             //Contador para pulsos de encoder del motor izquierdo para la w. Se actualiza cada 100 ms
float pulsosDcha_odom=0.0;          //Contador para pulsos de encoder del motor derecho para la odometría. Se actualiza cada 100 ms
float pulsosDcha_w=0.0;             //Contador para pulsos de encoder del motor derecho para la w. Se actualiza cada 100 ms
float pulsosIzda_ant_odom=0.0;      //Contador para pulsos de encoder del motor izquierdo del instante anterior para la odometría. Se actualiza cada 100 ms
float pulsosIzda_ant_w=0.0;         //Contador para pulsos de encoder del motor izquierdo del instante anterior para la w. Se actualiza cada 100 ms
float pulsosDcha_ant_odom=0.0;      //Contador para pulsos de encoder del motor derecho del instante anterior para la odometría. Se actualiza cada 100 ms
float pulsosDcha_ant_w=0.0;         //Contador para pulsos de encoder del motor derecho del instante anterior para la w. Se actualiza cada 100 ms
float wMotorIzda=0.0;               //Velocidad angular del motor izquierdo [rpm]
float wMotorDcha=0.0;               //Velocidad angular del motor derecho [rpm]
float wMotorIzda_ant=0.0;           //Velocidad angular del motor izquierdo en el instante anterior [rpm]
float wMotorDcha_ant=0.0;           //Velocidad angular del motor derecho en el instante anterior [rpm]
float resolucionEncoder=384.0;      //Resolución de los encoders: 8 PPR * 48:1 = 384 [pulsos por revolución del eje de la rueda] 
const float resolIzda = 363.0;
const float resolDcha = 362.0;
bool sentidoGiroMotorIzda=0;        //Sentido de giro del motor izquierdo. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
bool sentidoGiroMotorDcha=0;        //Sentido de giro del motor derecho. Usaremos los #define de arriba: izquierda = antihorario = 0; derecha = horario = 1
float estadoEncoder=0.0;            //Estado de la máquina de estados para elegir la referencia del control en bucle abierto y en bucle cerrado
float refVelocidadIzda=50.0;        //Referencia de velocidad angular para la rueda izquierda en el bucle cerrado
float refVelocidadDcha=50.0;        //Referencia de velocidad angular para la rueda derecha en el bucle cerrado
bool sentGiroIzq = avanza;          //Sentido de giro de la rueda izquierda
bool sentGiroDer = avanza;          //Sentido de giro de la rueda derecha

float LizdaNoReset = 0.0;
float LdchaNoReset = 0.0;

void setup(){
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT); 
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);

  //PINES DE LOS IR
  pinMode(IRIzqExt, INPUT); 
  pinMode(IRIzq, INPUT); 
  pinMode(IRCen, INPUT); 
  pinMode(IRDer, INPUT); 
  pinMode(IRDerExt, INPUT);   

  //COMUNICACIONES Y TELEMETRÍA
  Serial.begin(9600);    //Comunicación por puerto serie arduino
  Serial1.begin(38400);  //Comunicación por bluetooth

  //Establecer estado inicial: dirección y sentido iniciales
  //Establecer direcciones iniciales
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
  //Detener motores como estado inicial
  analogWrite(ENA, 0); 
  analogWrite(ENB, 0);

  //INICIALIZACIÓN DE PINES E INTERRUPCIONES DE LOS ENCODERS
  pinMode(pinEncoderIzda,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderIzda),contadorPulsosIzda, CHANGE); //CHANGE); //El pin 3 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsosIzda
                                            //Interrumpirá cada vez que el pin 3 cambie de valor

  pinMode(pinEncoderDcha,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderDcha),contadorPulsosDcha, CHANGE); //CHANGE); //El pin 2 de interrupción (pin 20 = pinEncoderIzdo) puede interrumpir
                                            //Cuando lo haga, llamará a la función contadorPulsosDcha
                                            //Interrumpirá cada vez que el pin 2 cambie de valor
}

 
void loop() {

  //MEDICIÓN DE VELOCIDAD DE LOS DOS MOTORES  
  if (millis()-t5>=100){ //Cada 100 milisegundos, leer las variables pulsosIzda y pulsosDcha, calcular las velocidades, y resetear estas variables
    ActualizarOdometria();

    pulsosIzda_w = pulsosIzda;
    pulsosDcha_w = pulsosDcha;

    wMotorIzda = (pulsosIzda_w-pulsosIzda_ant_w)*(600.0)/(2.0*resolIzda);  //El controlador se diseñó para interrupciones tipo RISING, hemos cambiado a CHANGE para odometría más precisa, de ahí el dividido por 2.0
    wMotorDcha = (pulsosDcha_w-pulsosDcha_ant_w)*(600.0)/(2.0*resolDcha);  //El controlador se diseñó para interrupciones tipo RISING, hemos cambiado a CHANGE para odometría más precisa, de ahí el dividido por 2.0

    //Actualizar variables
    wMotorIzda_ant = wMotorIzda;
    wMotorDcha_ant = wMotorDcha;
    pulsosIzda_ant_w = pulsosIzda_w;
    pulsosDcha_ant_w = pulsosDcha_w;
    t5 = millis();
  }

  //FMS
  switch (FMS)
  {
    case INICIO:
      nodoAntX=0; 
      nodoAntY=0; 
      ori=1; 
      FMS=ESPERADESTINO; 
    break;     
    case ESPERADESTINO:
      if (orden!=CARACTERVACIO && !((orden < '1') || (orden > '6')))
        {
          nodoDestX=fil[orden-'0'-1]; 
          nodoDestY=col[orden-'0'-1]; 
          FMS = PLANIFICADORTRAYECTORIA; 
        }
      
      orden = CARACTERVACIO; 
    break; 
    case PLANIFICADORTRAYECTORIA:
      contX=nodoDestX-nodoAntX; 
      contY=nodoDestY-nodoAntY; 
      FMS = XDESPLAZAMIENTO;  
    break; 
    case XDESPLAZAMIENTO:
      //FMS Desplazamiento
      switch (FMSDesp)
      {
        case GIRAORI: 
          switch (FMSGiro)
          {
          case COMPROBACIONORI:
            oriDes = (contX > 0) + 3*(contX < 0) + oriDes*(contX==0); 
            if (ori == oriDes) FMSDesp = AVANZA; 
            else if (ori%2)
            {
              FMSGiro = APROXIMACIONX;
              Xprima = x; 
            } 
            else if (!(ori%2)) 
            {
              FMSGiro = APROXIMACIONY; 
              Yprima = y; 
            }
          break; 
          case APROXIMACIONX:
          
            recto(avanza, 85); 
            break; 
            if (abs(x-Xprima) >= Lx )     //Lx no def. Deitancia centro giro robot a morro
            {
              recto(avanza, detener); 
              FMSGiro = FRENADO; 
            }
          break; 
          case APROXIMACIONY:
            recto(avanza, 85); 
            if (abs(y-Yprima) >= Ly )     //Ly no def. Deitancia centro giro robot a morro
            {
              recto(avanza, detener); 
              FMSGiro = FRENADO; 
            }

          break; 
          case FRENADO:
              if  (wMotorDcha == 0 && wMotorIzda == 0) 
              {
                FMSGiro = PIVOTEAR; 
                Xprima = 0.0; 
                Yprima = 0.0; 
                refVelocidadIzda = velPivote;
                refVelocidadDcha = -velPivote;
              }
          break; 
          case PIVOTEAR:
            if (lecturaIRDer)
              if (ori < 4) ori++; 
              else ori = 1; 
            if ((ori == oriDes) && lecturaIRCen)
            {
              recto(avanza,detener); 
              FMSGiro = COMPROBACIONORI; 
              FMSDesp = AVANZA; 
            }
          break; 
          }
      break; 
        case AVANZA: 
          //MEDIDA DE SENSORES IR
          medidaIRIzqExt  = lecturaIRIzqExt; 
          medidaIRIzq = lecturaIRIzq; 
          medidaIRCen = lecturaIRCen; 
          medidaIRDer = lecturaIRDer; 
          medidaIRDerExt  = lecturaIRDerExt;
          sensoresIR =  100*medidaIRIzq + 10*medidaIRCen + medidaIRDer; 

          //CORRECION DIRECCIÓN
          switch (sensoresIR){
            case 0:
              if (t8 == 0) t8 = millis(); 
              else 
              {
                if (millis()-t8 > 3000) FMS  = ERROR; 
              }

            break;                 
            case 1:
              giro(derecha, avanza,  150);
              t8 = 0; 
            break;               
            case 10:
              recto(avanza, 150);
              Serial1.println("LOREM IPSUM");
              t8 = 0; 
            break;               
            case 100:
              giro(izquierda, avanza,  150);
              t8 = 0;
            break;               
            case 110:
              giroBrusco(izquierda, avanza, 150); 
              t8 = 0;
            break;               
            case 11:
              giroBrusco(derecha, avanza, 150);
              t8 = 0;
            break;
            default:
              Serial1.println("DEFAULT");
            break;                 
          }
          if (medidaIRDerExt || medidaIRIzqExt) 
          {
            while ( !lecturaIRDerExt && !lecturaIRIzqExt);
            nodoAntX = nodoAntX + sign(contX)*1; 
            contX = contX - sign(contX)*1; 
            FMSDesp = COMPROBACIONODOM;  
            recto(avanza,detener);
          }
        break; 
        case COMPROBACIONODOM: 
            XDes = mapaOdomX[nodoAntX][nodoAntY]; 
            YDes = mapaOdomY[nodoAntX][nodoAntY];
            if ((abs(x-XDes) > 0.005) || (abs(y-YDes) > 0.005)) FMSDesp = CALIBRACION;
            else FMSDesp = ODOMCALIBRADA; 
        break; 
        case ODOMCALIBRADA: 
            if (contX == 0) FMS = YDESPLAZAMIENTO; 
            FMSDesp = GIRAORI;
        break; 
        case CALIBRACION:
            x = mapaOdomX[nodoAntX][nodoAntY]; 
            y = mapaOdomY[nodoAntX][nodoAntY]; 
            FMSDesp = ODOMCALIBRADA; 

        break; 
      }

    break; 
    case YDESPLAZAMIENTO:
      //FMS Desplazamiento
  
      switch (FMSDesp)
      {
        case GIRAORI: 
          if (ori == oriDes)
            FMSDesp = AVANZA;
          else 
            {
              if (ori%2)  
                {
                  if (Xprima == 0) Xprima = x; 
                  else 
                  {
                    recto(avanza, 85); 
                    if (abs(x-Xprima) >= Lx )     //Lx no def. Deitancia centro giro robot a morro
                      {
                        recto(avanza, detener); 
                        while (!(wMotorDcha == 0 && wMotorIzda == 0));

                      }
                  }
                }
              else
              {
                  if (Yprima == 0) Yprima = y; 
                  else 
                  {
                    recto(avanza, 85); 
                    if (abs(y-Yprima) >= Ly )     //Ly no def. Deitancia centro giro robot al lado
                      {
                        recto(avanza, detener); 
                        while (!(wMotorDcha == 0 && wMotorIzda == 0));

                      }
                  }
              } 
            switch (FMSGiro)
            {
            case COMPROBACIONORI:
              oriDes = (contY > 0) + 3*(contY < 0) + oriDes*(contY==0); 
              if (ori == oriDes) FMSDesp = AVANZA; 
              else if (ori%2)
              {
                 FMSGiro = APROXIMACIONX;
                Xprima = x; 
              } 
              else if (!(ori%2)) 
              {
                FMSGiro = APROXIMACIONY; 
                Yprima = y; 
              }
            break; 
            case APROXIMACIONX:
              recto(avanza, 85); 
               if (abs(x-Xprima) >= Lx )     //Lx no def. Deitancia centro giro robot a morro
              {
                recto(avanza, detener); 
                FMSGiro = FRENADO; 
              }
            break; 
            case APROXIMACIONY:
              recto(avanza, 85); 
               if (abs(y-Yprima) >= Ly )     //Ly no def. Deitancia centro giro robot a morro
              {
                recto(avanza, detener); 
                FMSGiro = FRENADO; 
              }

            break; 
            case FRENADO:
                if  (wMotorDcha == 0 && wMotorIzda == 0) 
                {
                  FMSGiro = PIVOTEAR; 
                  Xprima = 0.0; 
                  Yprima = 0.0; 
                  refVelocidadIzda = velPivote;
                  refVelocidadDcha = -velPivote;
                }

            break; 
            case PIVOTEAR:
              if (lecturaIRDer) 
                if (ori < 4) ori++; 
                else ori = 1; 
              if ((ori == oriDes) && lecturaIRCen)
              {
                recto(avanza,detener); 
                FMSGiro = COMPROBACIONORI; 
                FMSDesp = AVANZA; 
              }

            break; 
            }
            }
        break; 
        case AVANZA: 
          //MEDIDA DE SENSORES IR
          medidaIRIzqExt  = lecturaIRIzqExt; 
          medidaIRIzq = lecturaIRIzq; 
          medidaIRCen = lecturaIRCen; 
          medidaIRDer = lecturaIRDer; 
          medidaIRDerExt  = lecturaIRDerExt;
          sensoresIR =  100*medidaIRIzq + 10*medidaIRCen + medidaIRDer; 

          //CORRECION DIRECCIÓN
          switch (sensoresIR){
            case 000:
              if (t8 == 0) t8 = millis(); 
              else 
              {
                if (millis()-t8 > 3000) FMS  = ERROR; 
              }

            break;                 
            case 001:
              giro(derecha, avanza,  85);
              t8 = 0; 
            break;               
            case 010:
              recto(avanza, 85);
              t8 = 0; 
            break;               
            case 100:
              giro(izquierda, avanza,  85);
              t8 = 0;
            break;               
            case 110:
              giroBrusco(izquierda, avanza, 85); 
              t8 = 0;
            break;               
            case 011:
              giroBrusco(derecha, avanza, 85);
              t8 = 0;
            break;                 
          }
          if (medidaIRDerExt || medidaIRIzqExt) 
          {
            nodoAntY = nodoAntY + sign(contY)*1; 
            contY = contY - sign(contY)*1; 
            FMSDesp = COMPROBACIONODOM;  
            recto(avanza, detener); 
          }
        break; 
        case COMPROBACIONODOM: 
            XDes = mapaOdomX[nodoAntX][nodoAntY]; 
            YDes = mapaOdomY[nodoAntX][nodoAntY];
            if ((abs(x-XDes) > 0.005) || (abs(y-YDes) > 0.005)) FMSDesp = CALIBRACION;
            else FMSDesp = ODOMCALIBRADA; 
        break; 
        case ODOMCALIBRADA: 
            if (contY == 0) FMS = OBJETIVO; 
            FMSDesp = GIRAORI;
        break; 
        case CALIBRACION:
            x = mapaOdomX[nodoAntX][nodoAntY]; 
            y = mapaOdomY[nodoAntX][nodoAntY]; 
            FMSDesp = ODOMCALIBRADA; 

        break; 
      }


    break; 
    case OBJETIVO:
      FMS = ESPERADESTINO; 
      FMSDesp = AVANZA; 
      FMSGiro = COMPROBACIONORI; 
      contX = 0; 
      contY = 0; 
      Xprima = 0; 
      Yprima = 0; 
    break; 
    case ERROR:   //En case de algún error, el vehículo se detiene automáticamente
      recto(avanza, detener); 
    break; 
    default: 
      FMS = ERROR; 
    break; 

  }
 
  //ACTUACIÓN: CONDICIONADA A QUE EL MODO SELECCIONADO SEA 'TRUE' (PULSAR 't' PARA ALTERNAR): SI NO, DETENER EL COCHE
  if(modo){ /*                         
    //Motor izquierdo
    if (UrealIzq > 0) MotorIzquierdo(izquierda,UrealIzq);
    else if (UrealIzq < 0) MotorIzquierdo(derecha,-UrealIzq); 
    else MotorIzquierdo(avanza,detener); 
    //Motor derecho
    if (UrealDer > 0) MotorDerecho(izquierda,UrealDer);
    else if (UrealDer < 0) MotorDerecho(derecha,-UrealDer); 
    //else MotorDerecho(avanza,detener);*/ 
  }
  else{
    MotorIzquierdo(avanza,detener);
    MotorDerecho(avanza,detener); 
  }

  //LECTURA DE TECLADO
  if (millis()- t2 >= 100){                               //Cada 100 ms, leer el teclado
   if (recepcion =='t'){
     telemetria = !telemetria;                             //Comenzar el experimento enviando los datos de la telemetría
     modo = !modo;                                         //Se alterna el modo (que el control actúe o no)
    }
   
    else orden =  recepcion;                               //Si no es de la calibración en línea, es una orden para el sistema
    recepcion = 185;                                       //Resetear la tecla en memoria cuando no se esté pulsando
    t2 = millis();
  }
  //"TELEMETRÍA"
  
  //Leer de Putty
     if(Serial1.available()>0){
        recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  Serial1.print("  x = ");
  Serial1.print(x);
  Serial1.print("  y = ");
  Serial1.print(y);
  Serial1.print("  phi(º) = ");
  Serial1.print(Phi*180.0/PI);
  Serial1.print("  FMS = ");
  Serial1.print(fms[FMS]);
  Serial1.print("  FMSDesp = ");
  Serial1.print(fmsDesp[FMSDesp]);
  Serial1.print("  FMSGiro = ");
  Serial1.print(fmsGiro[FMSGiro]);
  Serial1.print("  ori = ");
  Serial1.print((int) ori);
  Serial1.print("  oriDes = ");
  Serial1.print((int) oriDes );
  Serial1.print("  ContX = ");
  Serial1.print((int) contX);
  Serial1.print(" nodoActual = ");
  Serial1.print((int) nodoAntX);
  Serial1.print((int) nodoAntY);
  Serial1.print("  nodoDes = ");
  Serial1.print((int) nodoDestX);
  Serial1.print((int) nodoDestY);  
  Serial1.print(" DifLx = "); 
  Serial1.print(abs(x-Xprima));
  Serial1.print(" Sensores =  ");
  Serial1.print(10000*lecturaIRIzqExt+1000*lecturaIRIzq+100*lecturaIRCen+10*lecturaIRDer+1*lecturaIRDerExt);
  
  
  Serial1.println();
  
  //TELEMETRÍA
  /*
  //Leer de Putty
  if(Serial1.available()>0){
    recepcion = Serial1.read();      
  }
  
  //Escribir en Putty
  if(telemetria){   
  Serial1.print(t-t4);                         //dat1: tiempo entre actuaciones
  Serial1.print(",");
  
  //Sensores
  Serial1.print(wMotorIzda);                   //dat2: velocidad angular del motor izquierdo
  Serial1.print(",");
  Serial1.print(wMotorDcha);                   //dat3: velocidad angular del motor derecho
  Serial1.print(",");
  Serial1.print(refActualIzda);                //dat4: referencia FILTRADA de velocidad angular para motor izquierdo
  Serial1.print(",");
  Serial1.print(refActualDcha);                //dat5: referencia FILTRADA de velocidad angular para motor derecha
  Serial1.print(",");
  Serial1.print(x);                            //dat6: distancia recorrida en el eje x
  Serial1.print(",");
  Serial1.print(y);                            //dat7: distancia recorrida en el eje y
  Serial1.print(",");
  Serial1.print(Phi);                          //dat8: orientación del robot
  Serial1.print(",");
  Serial1.print(incX);                         //dat9: incremento de distancia en el eje x
  Serial1.print(",");
  Serial1.print(incY);                         //dat10: incremento de distancia en el eje y
  Serial1.print(",");
  Serial1.print(incPhi);                       //dat11:  incremento de orientación 
  Serial1.print(",");
  Serial1.print(pulsosIzda);                   //dat12: pulsos en el encoder izquierdo desde el arranque del programa
  Serial1.print(",");
  Serial1.print(pulsosDcha);                   //dat13: pulsos en el encoder derecho desde el arranque del programa
  Serial1.print(",");
  
  //Actuadores
  Serial1.print(UrealIzq);                     //dat14: escritura del actuador izquierdo (pwm)
  Serial1.print(",");
  Serial1.print(UrealDer);                     //dat15: escritura del actuador derecho (pwm)
  Serial1.print(";");
  Serial1.println();
  }
  */
}                                             //Fin void loop() 
  

//DEFINICIÓN DE LAS FUNCIONES
int distanciaUS(int TriggerPin, int EchoPin) { //Función para medir la distancia por ultrasonidos
   float duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);              //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);             //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);          //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;        //convertimos a distancia, en cm
   return distanceCm;
}

void MotorIzquierdo(bool sentGiro, int pwm){   //Función para controlar directamente el motor izquierdo
  if (sentGiro == avanza)
    {
    digitalWrite(IN3, HIGH); 
    digitalWrite(IN4, LOW); 
    sentGiroIzq = avanza; 
    }
  else 
    { 
    digitalWrite(IN3, LOW); 
    digitalWrite(IN4, HIGH);
    sentGiroIzq = retrocede;
    }
  analogWrite(ENB, pwm); 
}
void MotorDerecho(bool sentGiro, int pwm)      //Función para controlar directamente el motor derecho
{
  if (sentGiro == avanza)
    {
    digitalWrite(IN1, HIGH); 
    digitalWrite(IN2, LOW); 
    sentGiroDer = avanza; 
    }
  else 
    { 
    digitalWrite(IN1, LOW); 
    digitalWrite(IN2, HIGH);
    sentGiroDer = retrocede; 
    }
  analogWrite(ENA, pwm); 
}
void recto(bool sentido,int pwm)                  //Función para hacer que el coche avance o retroceda a cierta velocidad
{
  MotorIzquierdo(sentido, pwm); 
  MotorDerecho(sentido, 0.902*pwm);               //Con factor de corrección para que los motores vayan a la misma velocidad realmente 
}
void giro(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.7;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void giroBrusco(bool sentGiro, bool sentAvance,int pwm) //Función para hacer que el gire avanzando hacia izda o dcha a cierta velocidad
{
  float factorDiferencial = 0.45;                  //Girar avanzando => que las dos ruedas giran en el mismo sentido, pero la rueda
  if (sentGiro == izquierda)                      //del lado hacia el que giramos avanza más lento.  Ídem para retroceder
    {
      MotorIzquierdo(sentAvance, pwm*factorDiferencial); 
      MotorDerecho(sentAvance, pwm); 
    }
  else if (sentGiro == derecha)
    {
      MotorIzquierdo(sentAvance, pwm); 
      MotorDerecho(sentAvance, pwm*factorDiferencial); 
    }
  
}
void pivota(bool sentido, int pwm)                //Función para que avanzando hacia izda o dcha a cierta velocidad
{
  if (sentido == derecha)                         //Una rueda gira en un sentido, y la otra en el otro sentido a la misma velocidad
    {
      MotorIzquierdo(avanza, pwm); 
      MotorDerecho(retrocede, pwm); 
    }
  else if (sentido == izquierda)
    {
      MotorDerecho(avanza, pwm); 
      MotorIzquierdo(retrocede, pwm); 
    }
}

void contadorPulsosIzda(){              //Rutina de interrupción para contar número de pulsos de los encoders
    if (sentGiroIzq == avanza)
    {
      pulsosIzda++;
    }
    else
    {
      pulsosIzda--;
    }
}
void contadorPulsosDcha(){              //Rutina de interrupción para contar número de pulsos de los encoders

    if (sentGiroDer == avanza)
    {
      pulsosDcha++;
    }
    else
    {
      pulsosDcha--;
    }
}

void ActualizarOdometria(){                           //Funcion que actualiza la odometría del robot
  float Lizda=0.0;
  float Ldcha=0.0;
  float Lcentro=0.0;
  
  pulsosIzda_odom = pulsosIzda;
  pulsosDcha_odom = pulsosDcha;
  

  //ODOMETRÍA POR DISTANCIA RECORRIDA

  Lizda = (pulsosIzda_odom-pulsosIzda_ant_odom)*2.0*c*PI/resolIzda;
  Ldcha = (pulsosDcha_odom-pulsosDcha_ant_odom)*2.0*c*PI/resolDcha;
  Lcentro = (Lizda+Ldcha)/2.0;


  incPhi = (Ldcha-Lizda)/b; 
  incX = Lcentro*cos(Phi); 
  incY = Lcentro*sin(Phi); 


  //CALCULAR ODOMETRÍA CON LOS INCREMENTOS OBTENIDOS
  x = x_ant + incX;
  y = y_ant + incY;
  Phi = Phi_ant + incPhi;
  //Phi = atan2(sin(Phi),cos(Phi));    //Mantener Phi entre [-180,180]
  while(Phi>=360.0*(PI/180.0))Phi = Phi-360.0*(PI/180.0);
  while(Phi<0.0*(PI/180.0))  Phi = Phi+360.0*(PI/180.0);
  
  //ACTUALIZACIÓN  
  x_ant = x;
  y_ant = y;
  Phi_ant = Phi; 
  
  pulsosIzda_ant_odom = pulsosIzda_odom;
  pulsosDcha_ant_odom = pulsosDcha_odom;  
  inc_k = (millis()-t7)/1000.0;
  t7 = millis();
}

void modo6(){   
    //Filtrado de la referencia
    if (modo){
      refActualIzda = alpha*refVelocidadIzda + (1-alpha)*refAntIzda;
      refActualDcha = alpha*refVelocidadDcha + (1-alpha)*refAntDcha;
    }
    else{ 
      refActualIzda = 0.0;
      refAntIzda = 0.0;
      refActualDcha = 0.0;
      refAntDcha = 0.0;
    }

    //Cálculo del error para los dos controladores:
    ekIzq = refActualIzda - wMotorIzda;
    ekDer = refActualDcha - wMotorDcha;

    //Implementación de controladores PID
    ukIzq = KpIzq * ekIzq + (Int_err_Izq / TiIzq) + TdIzq * ((ekIzq - ek1Izq) / Tm);  //PID Izquierdo
    ukDer = KpDer * ekDer + (Int_err_Der / TiDer) + TdDer * ((ekDer - ek1Der) / Tm);  //PID Derecho  

    //Zona muerta y saturación del control izquierdo: LA ZONA MUERTA ES LA MISMA QUE LA DE LOS MODOS 1 AL 4: si no hay un mínimo de Ueq,
    //el motor no puede tener velocidad no nula
    if(ukIzq>=0)Uk_ssIzq = ukIzq; //Salvar zona muerta por la zona positiva del control izquierdo sumando señal de equilibrio
    if(ukIzq<0)Uk_ssIzq =  ukIzq; //Salvar zona muerta por la zona negativa del control izquierdo restando señal de equilibrio
    //Saturación del controlador IZQUIERDO [Umin = -180; Umax = 180]
    if (Uk_ssIzq < Umin)UrealIzq = Umin;
    else if (Uk_ssIzq > Umax)UrealIzq = Umax;
    else UrealIzq = Uk_ssIzq; 
    if(Uk_ssIzq<Umax && Uk_ssIzq>Umin)Int_err_Izq += ekIzq * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Zona muerta y saturación del control derecho
    if(ukDer>=0)Uk_ssDer = ukDer; //No salvamos la zona muerta en este experimento
    if(ukDer<0)Uk_ssDer = ukDer;  //No salvamos zona muerta en este experimento
    //Saturación del controlador DERECHO [Umin = -180; Umax = 180]
    if (Uk_ssDer < Umin)UrealDer = Umin;
    else if (Uk_ssDer > Umax)UrealDer = Umax;
    else UrealDer = Uk_ssDer; 
    if(Uk_ssDer<Umax && Uk_ssDer>Umin)Int_err_Der += ekDer * Tm; //Anti-windup: sólo para cuando tenemos efecto integral

    //Actualizar variables
    ek1Izq = ekIzq;
    ek1Der = ekDer;
    refAntIzda = refActualIzda;
    refAntDcha = refActualDcha;
    t4 = t;
    t = millis();
}
